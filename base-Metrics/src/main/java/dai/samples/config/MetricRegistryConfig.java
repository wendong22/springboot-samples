package dai.samples.config;

import com.codahale.metrics.MetricRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author daify
 * @date 2022-05-06
 */
@Configuration
public class MetricRegistryConfig {

    @Bean
    public MetricRegistry metricRegistry() {
        return new MetricRegistry();
    }
}
