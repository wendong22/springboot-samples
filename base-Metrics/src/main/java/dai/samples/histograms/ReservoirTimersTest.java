package dai.samples.histograms;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SlidingTimeWindowReservoir;
import com.codahale.metrics.Timer;
import dai.samples.metrics.MetricBase;
import dai.samples.metrics.TimersTest;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author daify
 * @date 2022-05-07
 */
public class ReservoirTimersTest extends MetricBase {

    public static Random random = new Random();

    public static void main(String[] args) throws InterruptedException {
        initMetric();

        // 下面两种方式是正常代码和Lomdba格式
        /*MetricRegistry.MetricSupplier metricSupplier = new MetricRegistry.MetricSupplier() {
            @Override
            public Metric newMetric() {
                return new Timer(new SlidingTimeWindowReservoir(1,TimeUnit.MINUTES));
            }
        };*/
        MetricRegistry.MetricSupplier metricSupplier = () -> new Timer(new SlidingTimeWindowReservoir(1,TimeUnit.MINUTES));

        Timer timer = registry.timer(MetricRegistry.name(TimersTest.class,"get-latency"),metricSupplier);

        Timer.Context ctx;

        while(true){
            ctx = timer.time();
            Thread.sleep(random.nextInt(1000));
            ctx.stop();
        }
    }


}