package dai.samples.metrics;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 返回单一值，用来获取某些参数中当前的值
 * @author daify
 * @date 2022-05-06
 */
public class GaugesTest extends MetricBase{

    private static Queue<String> taskList = new LinkedList<String>();

    public static void main(String[] args) throws InterruptedException {
        initMetric();
        Gauge gauge = () -> taskList.size();
        // 注册统计内容
        registry.register(MetricRegistry.name(GaugesTest.class, "queue", "size"),gauge);

        while(true){
            Thread.sleep(1000);
            taskList.add("Job-xxx");
        }
    }
}
