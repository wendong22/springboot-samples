package dai.samples.metrics;

import com.codahale.metrics.ExponentiallyDecayingReservoir;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;

import java.util.Random;

/**
 * Histogram统计数据的分布情况。比如最小值，最大值，中间值，还有中位数，75百分位, 90百分位, 95百分位, 98百分位, 99百分位, 和 99.9百分位的值(percentiles)。
 * @author daify
 * @date 2022-05-06
 */
public class HistogramsTest extends MetricBase{

    public static Random random = new Random();

    public static void main(String[] args) throws InterruptedException {
        initMetric();
        // ExponentiallyDecayingReservoir(指数采样)
        // UniformReservoir(随机采样)
        // SlidingWindowReservoir(只存最近N条数据)
        // SlidingTimeWindowReservoir(指定时间窗口重置数据)
        // 注册一个Histogram
        Histogram histogram = new Histogram(new ExponentiallyDecayingReservoir());
        registry.register(MetricRegistry.name(HistogramsTest.class, "request", "histogram"), histogram);
        while(true){
            Thread.sleep(1000);
            histogram.update(random.nextInt(100000));
        }

    }
}
