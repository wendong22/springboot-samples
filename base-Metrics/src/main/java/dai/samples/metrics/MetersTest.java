package dai.samples.metrics;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;

import java.util.Random;

/**
 * Meter度量一系列事件发生的速率(rate)，例如TPS。Meters会统计最近1分钟，5分钟，15分钟，还有全部时间的速率。
 * @author daify
 * @date 2022-05-06
 */
public class MetersTest extends MetricBase{

    private static Random random = new Random();

    public static void main(String[] args) throws InterruptedException {
        initMetric();
        Meter meterTps = registry.meter(MetricRegistry.name(MetersTest.class,"request","tps"));
        while(true){
            request(meterTps,random.nextInt(5));
            Thread.sleep(1000);
        }
    }

    private static void request(Meter meter, int n){
        while(n > 0){
            System.out.println("request");
            meter.mark();
            n--;
        }
    }

}
