package dai.samples.metrics;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;

import java.util.concurrent.TimeUnit;

/**
 * @author daify
 * @date 2022-05-07
 */
public class MetricBase {

    public static MetricRegistry registry;

    public static void initMetric(){
        // 创建一个度量
        registry = new MetricRegistry();
        // 注册到控制台中
        ConsoleReporter reporter = ConsoleReporter.forRegistry(registry).build();
        // 每5秒输出一次结果
        reporter.start(5, TimeUnit.SECONDS);
    }

}
