package dai.samples.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

import java.util.Random;

/**
 * Timer其实是 Histogram 和 Meter 的结合， histogram 某部分代码/调用的耗时， meter统计TPS。
 * @author daify
 * @date 2022-05-06
 */
public class TimersTest extends MetricBase{

    public static Random random = new Random();

    public static void main(String[] args) throws InterruptedException {
        initMetric();
        Timer timer = registry.timer(MetricRegistry.name(TimersTest.class,"get-latency"));

        Timer.Context ctx;

        while(true){
            ctx = timer.time();
            Thread.sleep(random.nextInt(1000));
            ctx.stop();
        }
    }
}
