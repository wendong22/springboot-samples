package dai.samples.reporters;

import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import dai.samples.metrics.TimersTest;

import java.util.Random;

/**
 * @author daify
 * @date 2022-05-09
 */
public class JmxReporters {

    public static Random random = new Random();

    public static void main(String[] args) throws InterruptedException {
        // 创建一个度量
        MetricRegistry registry = new MetricRegistry();
        // 注册到控制台中
        JmxReporter reporter = JmxReporter.forRegistry(registry).build();
        reporter.start();

        Timer timer = registry.timer(MetricRegistry.name(TimersTest.class,"latency"));

        Timer.Context ctx;

        while(true){
            ctx = timer.time();
            Thread.sleep(random.nextInt(1000));
            ctx.stop();
        }
    }

}
