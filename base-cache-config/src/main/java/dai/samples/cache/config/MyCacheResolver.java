package dai.samples.cache.config;

import dai.samples.cache.dao.UserCacheConfigDao;
import dai.samples.cache.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.AbstractCacheResolver;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;

import java.util.Collection;

/**
 * value值的解析
 * @author daify
 * @date 2020-10-17
 */
public class MyCacheResolver extends AbstractCacheResolver {

    public MyCacheResolver() {
    }

    public MyCacheResolver(@Autowired CacheManager cacheManager) {
        super(cacheManager);
    }


    @Override
    protected Collection<String> getCacheNames(CacheOperationInvocationContext<?> context) {
        Object target = context.getTarget();
        if (target instanceof UserCacheConfigDao) {
            Object[] args = context.getArgs();
            Object userObj = args[0];
            if (userObj != null) {
                // 设置User的type为value
                context.getOperation().getCacheNames().clear();
                context.getOperation().getCacheNames().add(((User)userObj).getType());
            }
        }

        return context.getOperation().getCacheNames();
    }

}
