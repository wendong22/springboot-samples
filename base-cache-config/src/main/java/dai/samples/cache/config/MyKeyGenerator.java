package dai.samples.cache.config;

import dai.samples.cache.entity.User;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * key值的解析器
 * @author daify
 * @date 2020-10-17
 */
@Component(value = "myKeyGenerator")
public class MyKeyGenerator extends SimpleKeyGenerator implements KeyGenerator {


    @Override
    public Object generate(Object target, Method method, Object... params) {

        Object generate = super.generate(target, method, params);
        if (generate != SimpleKey.EMPTY) {
            Object param = params[0];
            return ((User)param).getId();
        }
        return generate;
    }
}
