package samples.cache.dao;

import dai.samples.cache.CacheConfigApplication;
import dai.samples.cache.dao.CacheDao;
import dai.samples.cache.dao.UserCacheConfigDao;
import dai.samples.cache.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * @author daify
 * @date 2020-10-17
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = CacheConfigApplication.class)
@EnableAutoConfiguration
@Slf4j
public class UserCacheDaoTest extends CacheConfigApplication {

    @Autowired
    private UserCacheConfigDao userCacheConfigDao;

    @Autowired
    private CacheDao cacheDao;


    /**
     * 通过cacheDao 设置的参数 使用userCacheConfigDao可以定位到
     * @return
     */
    @Test
    public void find() {
        User user = new User();
        user.setId(1L);
        user.setName("test");
        user.setType("user");
        cacheDao.clear(user);
        User user1 = cacheDao.findUser(user);
        Assert.assertTrue(StringUtils.isEmpty(user1.getName()));

        cacheDao.putUser(user);
        // 此时方法中没有提供任何value和key。但是自定义的参数解析会解析为key：1；value：user
        User user2 = userCacheConfigDao.findUser(user);
        Assert.assertTrue("test".equals(user2.getName()));
        log.info("cacheDao测试执行完毕");
    }


    /**
     * 通过cuserCacheConfigDao 设置的参数 使用acheDao 可以定位到
     * @return
     */
    @Test
    public void findOther() {
        User user = new User();
        user.setId(1L);
        user.setName("test");
        user.setType("user");
        userCacheConfigDao.clear(user);
        User user1 = userCacheConfigDao.findUser(user);
        Assert.assertTrue(StringUtils.isEmpty(user1.getName()));

        userCacheConfigDao.putUser(user);
        User user2 = cacheDao.findUser(user);
        Assert.assertTrue("test".equals(user2.getName()));
        log.info("userCacheConfigDao测试执行完毕");
    }


}
