package dai.samples.cache.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.cache.dao.CacheDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author daify
 * @date 2020-10-17
 */
@RestController
public class CacheController {

    @Autowired
    private CacheDao cacheDao;


    @RequestMapping("add/{key}")
    public String add(@PathVariable("key")String key) {
        String data = key + System.currentTimeMillis();
        Object o = cacheDao.putStr(data, key);
        return JSON.toJSONString(o);
    }

    @RequestMapping("get/{key}")
    public String get(@PathVariable("key")String key) {
        Object obj = cacheDao.find(key);
        return String.valueOf(obj);
    }

    @RequestMapping("del/{key}")
    public String del(@PathVariable("key")String key) {
        cacheDao.clear(key);
        return "OK";
    }

}
