package dai.samples.cache.dao;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * @author daify
 * @date 2020-10-13
 */
@Component
public class CacheDao {

    /**
     * 获取数据
     * @param key
     * @return
     */
    @Cacheable(value = "string", key = "#key")
    public String find(String key) {
        return "";
    }

    /**
     * 设置数据
     * @param data
     * @param key
     * @return
     */
    @CachePut(value = "string" ,key = "#key")
    public String putStr(String data,String key) {
        return data;
    }

    /**
     * 清空缓存
     * @param key
     */
    @CacheEvict(value = "string" ,key = "#key")
    public void clear(String key) {

    }
}
