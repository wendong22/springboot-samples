package dai.samples.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 *
 * @author daify
 * @date 2019-07-17 10:19
 **/
@TableName
@Data
public class User {

    @TableId(type = IdType.INPUT)
    private Long id;
    
    private String name;
    
    private Integer age;
    
    private String email;
}
