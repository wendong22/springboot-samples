package dai.samples.mybatis.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.plugin.*;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.sql.Statement;
import java.util.Properties;

/**
 *
 * @author daify
 * @date 2019-08-01
 **/
@Component
@Intercepts({
        // 拦截Executor的query的操作
        @Signature(
                type = ResultSetHandler.class,
                method = "handleResultSets",
                args = { Statement.class}
        )
})
@Slf4j
public class ExecutorResultSetHandlerPlugin implements Interceptor {

    /**
     * 进行拦截业务
     * @param invocation
     * @return
     * @throws Throwable
     */
    @Override 
    public Object intercept(Invocation invocation) throws Throwable {
        Method method = invocation.getMethod();
        log.info("ExecutorResultSetHandlerPlugin拦截内容，拦截类:{},拦截方法:{}",
                method.getDeclaringClass().getName(),method.getName());
        return invocation.proceed();
    }

    /**
     * 将这个类作为包装类假如拦截链
     * @param o
     * @return
     */
    @Override 
    public Object plugin(Object o) {
        return Plugin.wrap(o, this);
    }

    /**
     * 设置参数
     * @param properties
     * @return
     */
    @Override 
    public void setProperties(Properties properties) {

    }
}
