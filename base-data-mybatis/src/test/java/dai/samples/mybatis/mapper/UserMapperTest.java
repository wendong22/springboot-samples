package dai.samples.mybatis.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import dai.samples.mybatis.entity.Page;
import dai.samples.mybatis.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;


/**
 *
 * @author daify
 * @date 2019-08-01
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

    @Resource
    private UserMapper userMapper;

    @Test
    public void testPage() {
        Page<User> page = new Page(3,1);
        List<User> userPage = userMapper.selectMyPage(page, 30);
        System.out.println(userPage.size());
    }

    /**
     * 经过 
     * ExecutorQueryPlugin、
     * ExecutorParameterPlugin、
     * ExecutorResultSetHandlerPlugin拦截
     */
    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        User u = new User();
        u.setName("Tom");
        QueryWrapper<User> queryWrapper = new QueryWrapper <User>(u);
        List <User> userList = userMapper.selectList(queryWrapper);
        Assert.assertEquals(1, userList.size());
        userList.forEach(System.out::println);
    }

    @Test
    public void testInsert() {
        System.out.println(("----- testInsert method test ------"));
        User u = new User();
        u.setName("Dai");
        u.setAge(10);
        u.setEmail("123");
        userMapper.insert(u);
        
        QueryWrapper<User> queryWrapper = new QueryWrapper <User>(u);
        List <User> userList = userMapper.selectList(queryWrapper);
        Assert.assertEquals(1, userList.size());
        userList.forEach(System.out::println);
    }

    /**
     * 经过 
     * ExecutorUpdatePlugin、
     * ExecutorParameterPlugin、
     * ExecutorStatementHandlerPlugin 拦截
     */
    @Test
    public void testUpdate() {
        System.out.println(("----- selectAll method test ------"));
        User u = new User();
        u.setName("Tom");
        QueryWrapper<User> queryWrapper = new QueryWrapper <User>(u);
        List <User> userList = userMapper.selectList(queryWrapper);
        Assert.assertEquals(1, userList.size());
        
        User user = userList.get(0);
        user.setName("Tom1");
        userMapper.updateById(user);

        List <User> userList2 = userMapper.selectList(queryWrapper);
        Assert.assertEquals(0, userList2.size());
    }

    /**
     * 经过 
     * ExecutorUpdatePlugin、
     * ExecutorParameterPlugin、
     * ExecutorStatementHandlerPlugin 拦截
     */
    @Test
    public void testDelete() {
        System.out.println(("----- selectAll method test ------"));
        User u = new User();
        u.setName("Tom");
        QueryWrapper<User> queryWrapper = new QueryWrapper <User>(u);
        userMapper.delete(queryWrapper);
        List <User> userList = userMapper.selectList(queryWrapper);
        Assert.assertEquals(0, userList.size());
    }
    
}