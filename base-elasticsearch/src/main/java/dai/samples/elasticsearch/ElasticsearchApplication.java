package dai.samples.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author daify
 * @date 2019-07-17 15:42
 **/
@SpringBootApplication
public class ElasticsearchApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchApplication.class,args);
    }
}
