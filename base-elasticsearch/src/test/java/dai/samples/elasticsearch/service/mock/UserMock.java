package dai.samples.elasticsearch.service.mock;

import dai.samples.elasticsearch.entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author daify
 * @date 2019-08-04
 */
public class UserMock {

    private static String tag[] = new String[] {"武汉小吃","中国小吃","特色小吃","地方美食","地方风俗","武汉天气","武汉美食","武汉建筑"};

    public static User getOne() {
        return getUser(1).get(0);
    }


    public static List<User> getList(Integer num) {
        return getUser(num);
    }


    private static List<User> getUser(Integer num) {
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            User user = new User();
            user.setId(Long.valueOf(i+1));
            user.setDescription("这是第" + (i+1 ) + "个用户");
            user.setName("第" + (i+1) + "个用户");
            user.setScore(i+1);
            if (i%2 == 0) {
                user.setGroup("一");
            } else {
                user.setGroup("二");
            }
            userList.add(user);
            if (i < tag.length) {
                user.setTag(tag[i]);
            } else {
                user.setTag(tag[0]);
            }
        }
        return userList;
    }

}
