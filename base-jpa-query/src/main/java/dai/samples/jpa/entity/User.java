package dai.samples.jpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 用户信息
 * @author daify
 **/
@Data
@Entity
@Table(name = "cascade_user")
public class User {

    @Id
    @GeneratedValue
    private Long id;
    
    private String name;
    
    private Integer age;
    
    private Double point;

    /**
     * 多对多配置演示
     */
    @ManyToMany(
            cascade = {},
            fetch = FetchType.EAGER)
    @JoinTable(
            // 定义中间表的名称
            name = "cascade_user_role_table",
            // 定义中间表中关联User表的外键名
            joinColumns = {
                    @JoinColumn(name = "user_id",
                            referencedColumnName = "id")
            },
            // 定义中间表中关联OrderItem表的外键名
            inverseJoinColumns = {
                    @JoinColumn(name = "role_id",
                            referencedColumnName = "id")
            }
    )
    private Set <UserRole> roles;

    @ManyToMany(
            targetEntity = UserTag.class,
            cascade = {},
            fetch = FetchType.EAGER)
    // 假如不定义，jpa会生成“表名1”+“_”+“表名2”的中间表
    @JoinTable(
            // 定义中间表的名称
            name = "cascade_user_tag_table",
            // 定义中间表中关联User表的外键名
            joinColumns = {
                    @JoinColumn(name = "user_id",
                            referencedColumnName = "id")
            },
            // 定义中间表中关联UserTag表的外键名
            inverseJoinColumns = {
                    @JoinColumn(name = "tags_id",
                            referencedColumnName = "id")
            }
    )
    private Set<UserTag> tags = new HashSet <>();

    /**
     * CascadeType包含的类别  级联：给当前设置的实体操作另一个实体的权限
     *      CascadeType.ALL 级联所有操作
     *      CascadeType.PERSIST 级联持久化（保存）操作
     *      CascadeType.MERGE   级联更新（合并）操作
     *      CascadeType.REMOVE  级联删除操作
     *      CascadeType.REFRESH 级联刷新操作
     *      CascadeType.DETACH 级联分离操作,如果你要删除一个实体，但是它有外键无法删除，这个级联权限会撤销所有相关的外键关联。
     */
    @OneToOne(targetEntity = UserCart.class,
            cascade = CascadeType.ALL,
            mappedBy = "user")
    private UserCart userCart;

    @OneToOne(targetEntity = UserInfo.class,
            cascade = CascadeType.ALL)
    @JoinTable(name = "user_info_table",
            joinColumns = @JoinColumn(name="user_id"),
            inverseJoinColumns = @JoinColumn(name = "info_id"))
    private UserInfo userInfo;

    @OneToMany(mappedBy = "user",
            cascade = CascadeType.ALL)
    private List<UserAddress> userAddresses = new ArrayList <>();


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", point=" + point +
                ", roles=" + roles +
                ", tags=" + tags +
                ", userCart=" + userCart +
                ", userInfo=" + userInfo +
                ", userAddresses=" + userAddresses +
                '}';
    }
}
