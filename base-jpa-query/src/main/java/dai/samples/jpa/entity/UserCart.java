package dai.samples.jpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 用户购物车
 * @author daify
 **/
@Data
@Entity
@Table(name = "cascade_user_cart")
public class UserCart {

    @Id
    @GeneratedValue
    private Long id;
    
    private Integer count;
    
    private Double totalMoney;
    
    private Date updateTime;

    /**
     * CascadeType包含的类别  级联：给当前设置的实体操作另一个实体的权限
     *      CascadeType.ALL 级联所有操作
     *      CascadeType.PERSIST 级联持久化（保存）操作
     *      CascadeType.MERGE   级联更新（合并）操作
     *      CascadeType.REMOVE  级联删除操作
     *      CascadeType.REFRESH 级联刷新操作
     *      CascadeType.DETACH 级联分离操作,如果你要删除一个实体，但是它有外键无法删除，这个级联权限会撤销所有相关的外键关联。
     */
    @OneToOne(targetEntity = User.class,
            cascade = {},
            fetch = FetchType.LAZY)
    private User user;

    /**
     * 正常是会生成中间表的，
     * 但是添加了mappedBy进行反向关联，
     * 使用UserCartItem中的cartId属性来维持关系就可以避免出现中间表
     */
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "userCart")
    private List <UserCartItem> userCartItems;


    @Override
    public String toString() {
        return "UserCart{" +
                "id=" + id +
                ", count=" + count +
                ", totalMoney=" + totalMoney +
                ", updateTime=" + updateTime +
                ", user=" + user +
                ", userCartItems=" + userCartItems +
                '}';
    }
}
