package dai.samples.jpa.repository;

import dai.samples.jpa.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author daify
 * @date 2019-07-30 16:32
 **/
public interface UserRoleRepository
        extends JpaRepository <UserRole, Long> {

    /**
     * 使用预定义的HQL进行查询
     * @param roleName
     * @return
     */
    UserRole selectRoleByName(@Param("roleName") String roleName);

    /**
     * 使用预定义的SQL进行查询
     * @param roleName
     * @return
     */
    UserRole selectRoleByNameWithSql(@Param("roleName") String roleName);

    /**
     * 使用预定义的SQL进行查询
     * @param id
     * @return
     */
    UserRole selectRoleByIdWithSql(@Param("id") Long id);
}