package dai.samples.jpa.repository;

import com.alibaba.fastjson.JSON;
import dai.samples.JpaApplication;
import dai.samples.jpa.entity.UserRole;
import dai.samples.jpa.repository.mock.UserRoleMock;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author daify
 * @date 2019-07-31
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class JpaHqlUserRoleTest {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserAddressRepository userAddressRepository;

    @Autowired
    private UserCartRepository userCartRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserTagRepository userTagRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Before
    public void clearTable() {
        orderRepository.deleteAll();
        userAddressRepository.deleteAll();
        userCartRepository.deleteAll();
        userRepository.deleteAll();
        userRoleRepository.deleteAll();
        userTagRepository.deleteAll();
    }


    @Test
    public void testNamedQuery() {
        UserRole defRole = UserRoleMock.getDefRole();
        UserRole save = userRoleRepository.save(defRole);
        Assert.assertTrue(save.getId() != null);
        UserRole userRole = userRoleRepository.selectRoleByName(save.getRoleName());
        Assert.assertNotNull(userRole);
        log.info("name:{},userId:{}",userRole.getRoleName(),userRole.getId());
    }

    @Test
    public void testNamedNativeQuery1() {
        UserRole defRole = UserRoleMock.getDefRole();
        UserRole save = userRoleRepository.save(defRole);
        Assert.assertTrue(save.getId() != null);
        UserRole userRole = userRoleRepository.selectRoleByNameWithSql(save.getRoleName());
        Assert.assertNotNull(userRole);
        log.info("name:{},userId:{}",userRole.getRoleName(),userRole.getId());
    }

    @Test
    public void testNamedNativeQuery2() {
        UserRole defRole = UserRoleMock.getDefRole();
        UserRole save = userRoleRepository.save(defRole);
        Assert.assertTrue(save.getId() != null);
        UserRole userRole = userRoleRepository.selectRoleByIdWithSql(save.getId());
        Assert.assertNotNull(userRole);
        log.info(JSON.toJSONString(userRole));
    }
}
