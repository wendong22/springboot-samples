package dai.samples.jpa.repository;

import com.alibaba.fastjson.JSON;
import dai.samples.JpaApplication;
import dai.samples.jpa.bean.UserCartInfo;
import dai.samples.jpa.entity.User;
import dai.samples.jpa.entity.UserCart;
import dai.samples.jpa.repository.mock.UserCartMock;
import dai.samples.jpa.repository.mock.UserMock;
import dai.samples.jpa.utils.BeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试JPA使用SQL
 * @author daify
 * @date 2019-07-31
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class JpaHqlUserTest {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserAddressRepository userAddressRepository;

    @Autowired
    private UserCartRepository userCartRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserTagRepository userTagRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Before
    public void clearTable() {
        orderRepository.deleteAll();
        userAddressRepository.deleteAll();
        userCartRepository.deleteAll();
        userRepository.deleteAll();
        userRoleRepository.deleteAll();
        userTagRepository.deleteAll();
    }
    
    @Test
    public void testSelect() {
        User defUser1 = UserMock.getDefUser1();
        User save = userRepository.save(defUser1);
        Assert.assertTrue(save.getId() != null);
        User user = userRepository.selectUserById(save.getId());
        Assert.assertNotNull(user);
        log.info("name:{},userId:{}",user.getName(),user.getId());
    }

    @Test
    public void testSelectManyParams() {
        User defUser1 = UserMock.getDefUser1();
        User save = userRepository.save(defUser1);
        Assert.assertTrue(save.getId() != null);
        User user = userRepository.selectUserByIdAndName(save.getId(),save.getName());
        Assert.assertNotNull(user);
        log.info("name:{},userId:{}",user.getName(),user.getId());
    }

    @Test
    public void testCustomSql() {
        User defUser1 = UserMock.getDefUser1();
        userRepository.save(defUser1);
        UserCart defUserCart1 = UserCartMock.getDefUserCart1();
        defUserCart1.setUser(defUser1);
        userCartRepository.save(defUserCart1);

        User user1 = userRepository.selectUserById(defUser1.getId());
        UserCart userCart = userCartRepository.findById(defUserCart1.getId()).get();
        Assert.assertNotNull(user1);
        Assert.assertNotNull(userCart);
    }

    @Test
    public void testCustomResult() {
        User defUser1 = UserMock.getDefUser1();
        userRepository.save(defUser1);
        UserCart defUserCart1 = UserCartMock.getDefUserCart1();
        defUserCart1.setUser(defUser1);
        userCartRepository.save(defUserCart1);

        Object[] objects = userRepository.selectUserCartInfoById(defUser1.getId());
        Assert.assertNotNull(objects);
        Assert.assertNotNull(JSON.toJSONString(objects));
        List<Object[]> list = new ArrayList <>();
        list.add(objects);
        List <UserCartInfo> userCartInfos = BeanUtils
                .castEntity(list, UserCartInfo.class, new UserCartInfo());
        log.info("objects",JSON.toJSONString(userCartInfos));
        
    }
    
}
