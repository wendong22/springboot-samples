package dai.samples.jpa.repository.mock;


import dai.samples.jpa.entity.OrderItem;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daify
 * @date 2019-07-30 21:28
 **/
public class OrderItemMock {
    
    public static List<OrderItem> getAllDefOrder() {
        List<OrderItem> list = new ArrayList <>();
        list.add(getDefOrder1());
        list.add(getDefOrder2());
        list.add(getDefOrder3());
        return list;
    }
    

    public static OrderItem getDefOrder1() {
        OrderItem orderItem = new OrderItem();
        orderItem.setCount(20);
        orderItem.setMoney(200d);
        orderItem.setProductId("111");
        orderItem.setProductName("测试1");
        return orderItem;
    }

    public static OrderItem getDefOrder2() {
        OrderItem orderItem = new OrderItem();
        orderItem.setCount(40);
        orderItem.setMoney(400d);
        orderItem.setProductId("222");
        orderItem.setProductName("测试2");
        return orderItem;
    }

    public static OrderItem getDefOrder3() {
        OrderItem orderItem = new OrderItem();
        orderItem.setCount(60);
        orderItem.setMoney(600d);
        orderItem.setProductId("333");
        orderItem.setProductName("测试3");
        return orderItem;
    }
    
}
