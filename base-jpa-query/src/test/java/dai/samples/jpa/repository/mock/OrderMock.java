package dai.samples.jpa.repository.mock;

import dai.samples.jpa.entity.Order;

/**
 *
 * @author daify
 * @date 2019-07-30 21:19
 **/
public class OrderMock {
    
    public static Order getDefOrder1() {
        Order order = new Order();
        order.setCount(10);
        order.setMoney(100d);
        order.setOrderNo("N111");
        return order;
    }

    public static Order getDefOrder2() {
        Order order = new Order();
        order.setCount(20);
        order.setMoney(200d);
        order.setOrderNo("N222");
        return order;
    }

    public static Order getDefOrder3() {
        Order order = new Order();
        order.setCount(30);
        order.setMoney(300d);
        order.setOrderNo("N333");
        return order;
    }

    public static Order getDefOrder4() {
        Order order = new Order();
        order.setCount(40);
        order.setMoney(400d);
        order.setOrderNo("N444");
        return order;
    }
}
