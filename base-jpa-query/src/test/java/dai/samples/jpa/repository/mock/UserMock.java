package dai.samples.jpa.repository.mock;

import dai.samples.jpa.entity.User;

/**
 *
 * @author daify
 * @date 2019-07-30 20:36
 **/
public class UserMock {
    
    public static User getDefUser1 () {
        User user = new User();
        user.setAge(10);
        user.setName("测试用户1");
        user.setPoint(12.3D);
        return user;
    }

    public static User getDefUser2 () {
        User user = new User();
        user.setAge(20);
        user.setName("测试用户2");
        user.setPoint(22.3D);
        return user;
    }

    public static User getDefUser3 () {
        User user = new User();
        user.setAge(30);
        user.setName("测试用户3");
        user.setPoint(32.3D);
        return user;
    }

    public static User getDefUser4 () {
        User user = new User();
        user.setAge(40);
        user.setName("测试用户4");
        user.setPoint(42.3D);
        return user;
    }
}
