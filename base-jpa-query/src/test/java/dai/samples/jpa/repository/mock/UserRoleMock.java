package dai.samples.jpa.repository.mock;


import dai.samples.jpa.entity.UserRole;
import dai.samples.jpa.entity.UserTag;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daify
 * @date 2019-07-30 20:35
 **/
public class UserRoleMock {
    
    public static List<UserRole> getAllDefRole() {
        List<UserRole> roles = new ArrayList <>();
        roles.add(getDefRole());
        roles.add(getDefRole2());
        return roles;
    }

    public static List<UserTag> getAllDefTag() {
        List<UserTag> roles = new ArrayList <>();
        roles.add(getDefTag());
        roles.add(getDefTag2());
        return roles;
    }

    public static UserRole getDefRole() {
        UserRole role = new UserRole();
        role.setAuth("auth");
        role.setRoleName("测试角色");
        return role;
    }

    public static UserRole getDefRole2() {
        UserRole role = new UserRole();
        role.setAuth("auth2");
        role.setRoleName("测试角色2");
        return role;
    }

    public static UserTag getDefTag() {
        UserTag tag = new UserTag();
        tag.setTagName("测试tag");
        return tag;
    }

    public static UserTag getDefTag2() {
        UserTag tag = new UserTag();
        tag.setTagName("测试tag2");
        return tag;
    }
}
