package dai.samples.jpa.entity.base;

import lombok.Data;

import javax.persistence.*;

/**
 * 主键测试
 * @author daify
 * @date 2019-07-20
 */
@Data
@Entity
@Table(name = "base_key_table_bean")
public class KeyTableBean {

    /**
     * 使用生产方式。GenerationType
     * GenerationType
     * TABLE：容器指定用底层的数据表确保唯一,主键的值每次都是从指定的表中查询来获得
     * SEQUENCE：使用数据库德SEQUENCE列保证唯一（Oracle数据库通过序列来生成唯一ID）
     * IDENTITY：使用数据库的IDENTITY列保证唯一
     * AUTO：由容器挑选一个合适的方式来保证唯一
     * NONE：容器不负责主键的生成，由程序来完成
     */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE,generator = "table_key")
    @TableGenerator(name = "table_key",// 主键生成策略的名称
            table = "key_my_gen",// 生成策略所持久化的表名
            pkColumnName = "key_name",// 在持久化表中，该主键生成策略所对应键值的名称
            valueColumnName = "key_value",// 在持久化表中，该主键当前所生成的值
            pkColumnValue = "TABLE_KEY",// 生产策略所对应的主键的值
            initialValue = 10,// 初始值
            allocationSize = 1)// 每次主键值增加的大小
    private Long id;

    private String value;
}
