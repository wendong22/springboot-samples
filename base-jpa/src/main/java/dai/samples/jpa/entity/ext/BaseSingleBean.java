package dai.samples.jpa.entity.ext;

import lombok.Data;
import org.hibernate.annotations.DiscriminatorOptions;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * 测试DiscriminatorColumn
 * 单表继承
 * @author daify
 * @date 2019-07-20
 */
@Data
@Entity
@Table(name = "ext_base_single_bean")
/**继承策略,
 * SINGLE_TABLE表示继承关系的实体保存在一个表；
 * JOINED表示每个实体子类部分保存在各自的表；
 * TABLE_PER_CLASS表示每个实体类保存在各自的表*/
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
/**标识字段定义*/
@DiscriminatorColumn(name="table_type",
        discriminatorType = DiscriminatorType.STRING)
@DiscriminatorOptions(force=true)
/**该类的标识*/
@DiscriminatorValue("base")
public class BaseSingleBean {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    private BigDecimal balance;

}
