package dai.samples.jpa.entity.ext;

import lombok.Data;
import org.hibernate.annotations.DiscriminatorOptions;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * 多个entity对应同多张表，用一个字段区分  
 * @author daify
 * @date 2019-07-20
 */
@Data
@Entity
@Table(name = "ext_base_table_per_bean")
/**继承策略,
 * SINGLE_TABLE表示继承关系的实体保存在一个表；
 * JOINED表示每个实体子类部分保存在各自的表；
 * TABLE_PER_CLASS表示每个实体类保存在各自的表*/
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorOptions(force=true)
/**该类的标识*/
public class BaseTablePerBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    private BigDecimal balance;
}
