package dai.samples.jpa.entity.key;

import lombok.Data;

import javax.persistence.*;

/**
 * 主键测试
 * @author daify
 * @date 2019-07-20
 */
@Data
@Entity(name = "sequenceBean")
@Table(name = "key_sequence_bean")
public class KeySequenceBean {

    /**
     * 使用生产方式。GenerationType
     * GenerationType
     * TABLE：容器指定用底层的数据表确保唯一,主键的值每次都是从指定的表中查询来获得
     * SEQUENCE：使用数据库德SEQUENCE列保证唯一（Oracle数据库通过序列来生成唯一ID）
     * IDENTITY：使用数据库的IDENTITY列保证唯一
     * AUTO：由容器挑选一个合适的方式来保证唯一
     * NONE：容器不负责主键的生成，由程序来完成
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "sequence_key")
    @SequenceGenerator(
            name = "sequence_key",//生成器名字
            sequenceName = "key_sequence",// 数据库中运行创建名为 key_sequence的序列号
            initialValue = 15, // 主键开始的值
            allocationSize = 5 // 从序列分配序列号时要增加的量
    )
    private Long id;

    private String keyName;

    private String value;
}
