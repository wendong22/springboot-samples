package dai.samples.jpa.entity.key;

import lombok.Data;

import java.io.Serializable;

/**
 * 复合主键测试
 * @author daify
 * @date 2019-07-20
 */
@Data
public class KeysBean implements Serializable {

    private long id;

    private String keyId;
}
