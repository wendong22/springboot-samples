package dai.samples.jpa.entity.key;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author daify
 * @date 2019-07-29 16:26
 **/
@Data
@Entity
@Table(name = "key_test_keys")
@IdClass(value = KeysBean.class)
public class TestKeys implements Serializable {
    @Id
    private long id;
    @Id
    private String keyId;
    
    private String value;
}
