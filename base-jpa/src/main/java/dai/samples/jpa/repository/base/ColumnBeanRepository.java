package dai.samples.jpa.repository.base;

import dai.samples.jpa.entity.base.ColumnBean;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daify
 * @date 2019-07-29 14:51
 **/
public interface ColumnBeanRepository extends JpaRepository <ColumnBean, Long> {
}
