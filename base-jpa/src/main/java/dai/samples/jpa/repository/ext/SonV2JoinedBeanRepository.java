package dai.samples.jpa.repository.ext;

import dai.samples.jpa.entity.ext.SonV2JoinedBean;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author daify
 * @date 2019-07-30 9:09
 **/
public interface SonV2JoinedBeanRepository 
        extends JpaRepository <SonV2JoinedBean, Long> {
}
