package dai.samples.jpa.repository.base;

import dai.samples.jpa.JpaApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 *
 * @author daify
 * @date 2019-07-29 16:08
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class TestNameRepositoryTest {

    @Autowired
    TestNameRepository repository;

    @Before
    public void initTable() {
        repository.deleteAll();
    }

}