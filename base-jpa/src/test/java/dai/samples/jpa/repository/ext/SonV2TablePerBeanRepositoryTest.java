package dai.samples.jpa.repository.ext;

import com.alibaba.fastjson.JSON;
import dai.samples.jpa.JpaApplication;
import dai.samples.jpa.entity.ext.SonV2TablePerBean;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author daify
 * @date 2019-07-30 9:15
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class SonV2TablePerBeanRepositoryTest {

    @Autowired
    private SonV2TablePerBeanRepository repository;

    @Before
    public void initTable() {
        repository.deleteAll();
    }

    @Test
    public void add() {
        repository.save(getTestBean());
        List <SonV2TablePerBean> all = repository.findAll();
        log.info(JSON.toJSONString(all));
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 1);
    }

    private SonV2TablePerBean getTestBean() {
        SonV2TablePerBean bean = new SonV2TablePerBean();
        bean.setBalance(new BigDecimal(1.23));
        bean.setId(1L);
        return bean;
    }
}