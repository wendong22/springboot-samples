package dai.samples.jpa.repository.key;

import com.alibaba.fastjson.JSON;
import dai.samples.jpa.JpaApplication;
import dai.samples.jpa.entity.key.KeySequenceBean;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;


/**
 *
 * @author daify
 * @date 2019-07-29 16:12
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class KeySequenceBeanRepositoryTest {

    @Autowired
    private KeySequenceBeanRepository repository;

    @Before
    public void initTable() {
        repository.deleteAll();
    }

    @Test
    public void add() {
        KeySequenceBean bean = new KeySequenceBean();
        bean.setKeyName("12name");
        bean.setValue("12");
        repository.save(bean);
        KeySequenceBean bean2 = new KeySequenceBean();
        bean2.setKeyName("13name");
        bean2.setValue("13");
        repository.save(bean2);
        List <KeySequenceBean> all = repository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 2);
        // 实际中发现使用MYSQL，配置allocationSize 字段并没有生效
        log.info(JSON.toJSONString(all));
    }
}