package dai.samples.jpa.repository.key;

import com.alibaba.fastjson.JSON;
import dai.samples.jpa.JpaApplication;
import dai.samples.jpa.entity.key.KeysBean;
import dai.samples.jpa.entity.key.TestKeys;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 *
 * @author daify
 * @date 2019-07-29 16:12
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JpaApplication.class)
@Slf4j
public class TestKeysRepositoryTest {
    
    @Autowired
    private TestKeysRepository repository;

    @Before
    public void initTable() {
        repository.deleteAll();
    }

    @Test
    public void add() {
        TestKeys bean = new TestKeys();
        bean.setId(1L);
        bean.setKeyId("1");
        repository.save(bean);
        List <TestKeys> all = repository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 1);
        log.info(JSON.toJSONString(all));
    }

}