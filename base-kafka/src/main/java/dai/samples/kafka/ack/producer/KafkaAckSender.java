package dai.samples.kafka.ack.producer;

import com.alibaba.fastjson.JSON;
import dai.samples.kafka.common.config.KafkaConfig;
import dai.samples.kafka.common.entity.CustomMessage;
import dai.samples.kafka.common.entity.MyMessage;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * kafka消息发送者
 * @author daify
 */
@Component
public class KafkaAckSender {

    @Autowired
    private KafkaTemplate<String,Object> kafkaTemplate;

    public String sendStr(String messageStr,String topic,int max){
        long l = System.currentTimeMillis();
        for (int i = 0; i < max; i++) {
            MyMessage myMessage = new MyMessage();
            myMessage.setId(i);
            myMessage.setName(messageStr + i);
            myMessage.setCreateTime(l);
            CustomMessage message = new CustomMessage();
            message.setPayload(myMessage,topic);
            kafkaTemplate.send(message);
        }
        return messageStr;
    }


}
