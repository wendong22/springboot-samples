package dai.samples.kafka.common.entity;

import lombok.Data;

/**
 * 一个简单的Message模型
 * @author daify
 * @date 2020-04-27
 */
@Data
public class MyMessage {

    /**
     * long 类型
     */
    private long id;

    /**
     * name
     */
    private String name;

    /**
     * age
     */
    private int type;

    /**
     * 创建时间搓
     */
    private long createTime;

}
