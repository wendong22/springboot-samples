package dai.samples.kafka.common.entity;

import org.springframework.messaging.Message;

public interface SamplesMessage extends Message {

    String getTopic();
}
