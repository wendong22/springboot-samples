package dai.samples.kafka.transaction.consumer;

import com.alibaba.fastjson.JSON;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.Message;

/**
 * @author daify
 */
//@Component
@Log4j2
public class KafkaConsumerListener {


    @KafkaListener(topics = "transaction-test")
    public void onMessage2(Message message){
        Object payload = message.getPayload();
        log.info("transaction-test接收结果:{}",JSON.toJSONString(payload));
    }

}
