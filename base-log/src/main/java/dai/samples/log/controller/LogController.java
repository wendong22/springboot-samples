package dai.samples.log.controller;

import dai.samples.log.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author daify
 * @date 2019-07-18
 */
@RestController
@Slf4j
public class LogController {

    @Autowired
    private LogService logService;

    @RequestMapping(value = "infoTest",method = RequestMethod.GET)
    public String infoTest() {
        String str = logService.infoTest();
        log.info("INFO的日志输出：{}",str);
        return str;
    }

    @RequestMapping(value = "warnTest",method = RequestMethod.GET)
    public String warnTest() {
        String str = logService.warnTest();
        log.warn("INFO的日志输出：{}",str);
        return str;
    }

    @RequestMapping(value = "errorTest",method = RequestMethod.GET)
    public String errorTest() {
        String str = logService.errorTest();
        log.error("INFO的日志输出：{}",str);
        return str;
    }

    @RequestMapping(value = "debugTest",method = RequestMethod.GET)
    public String debugTest() {
        String str = logService.debugTest();
        log.debug("INFO的日志输出：{}",str);
        return str;
    }

    @RequestMapping(value = "traceTest",method = RequestMethod.GET)
    public String traceTest() {
        String str = logService.traceTest();
        log.trace("INFO的日志输出：{}",str);
        return str;
    }

}
