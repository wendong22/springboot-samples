package dai.samples.mongodb.bean;

import lombok.Data;

@Data
public class GroupVo {

    private String userId;

    private String type;

    private int num;

    private int count;
}
