package dai.samples.mongodb.bean;

import dai.samples.mongodb.entity.UserGroup;
import dai.samples.mongodb.entity.UserInfo;
import lombok.Data;

import java.util.List;

@Data
public class UserGroupVo extends UserGroup {

    private List<UserInfo> UserInfos;
}
