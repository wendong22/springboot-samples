package dai.samples.mongodb.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Objects;

@Data
@Document(collection = "Order")
public class Order {

    @Indexed
    private String id;

    private double totalMoney;

    private int totalProduct;

    private String userId;

    private List<OrderItem> orderItemList;

    private int type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return Double.compare(order.getTotalMoney(), getTotalMoney()) == 0 &&
            getTotalProduct() == order.getTotalProduct() &&
            getType() == order.getType() &&
            Objects.equals(getId(), order.getId()) &&
            Objects.equals(getUserId(), order.getUserId()) &&
            Objects.equals(getOrderItemList(), order.getOrderItemList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTotalMoney(), getTotalProduct(), getUserId(), getOrderItemList(), getType());
    }
}
