package dai.samples.mongodb.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Data
@Document(collection = "Product")
public class Product {

    @Indexed
    private String id;

    private String productName;

    private double productMoney;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Double.compare(product.getProductMoney(), getProductMoney()) == 0 &&
            getId().equals(product.getId()) &&
            getProductName().equals(product.getProductName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getProductName(), getProductMoney());
    }
}
