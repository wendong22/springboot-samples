package dai.samples.mongodb.mock;

import dai.samples.mongodb.entity.Order;
import dai.samples.mongodb.entity.OrderItem;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class OrderMock {

    public static Order createMock(int id, String userId, int type, List<OrderItem> orderItems) {
        AtomicInteger total = new AtomicInteger();
        AtomicReference<Double> totalMoney = new AtomicReference<>(0.0);
        orderItems.forEach(item -> {
            total.addAndGet(item.getTotal());
            totalMoney.updateAndGet(v -> v + item.getTotalMoney());
        });
        Order order = new Order();
        order.setId(String.valueOf(id));
        order.setOrderItemList(orderItems);
        order.setTotalMoney(totalMoney.get());
        order.setTotalProduct(total.get());
        order.setType(type);
        order.setUserId(userId);
        return order;
    }

    public static Order createMock(int id,int type) {
        Order order = new Order();
        order.setId(String.valueOf(id));
        order.setOrderItemList(null);
        order.setTotalMoney(id*10.0);
        order.setTotalProduct(id);
        order.setType(type);
        order.setUserId(null);
        return order;
    }

    public static Order createMock(int id,int type,String userId) {
        Order order = new Order();
        order.setId(String.valueOf(id));
        order.setOrderItemList(null);
        order.setTotalMoney(id*10.0);
        order.setTotalProduct(id);
        order.setType(type);
        order.setUserId(userId);
        return order;
    }
}
