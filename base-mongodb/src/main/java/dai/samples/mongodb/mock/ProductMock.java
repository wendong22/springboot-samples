package dai.samples.mongodb.mock;


import dai.samples.mongodb.entity.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductMock {

    public static Product createMock(int id) {
        Product product = new Product();
        product.setId(String.valueOf(id));
        product.setProductName("测试" + id);
        product.setProductMoney(id * 10.0);
        return product;
    }


    public static List<Product> createMockList(int num) {
        List<Product> rest = new ArrayList<>();
        for (int i = 1; i <= num; i++) {
            rest.add(createMock(i));
        }
        return rest;
    }
}
