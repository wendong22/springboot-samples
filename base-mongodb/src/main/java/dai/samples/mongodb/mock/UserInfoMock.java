package dai.samples.mongodb.mock;


import dai.samples.mongodb.entity.UserInfo;
import dai.samples.mongodb.entity.UserLogistics;

public class UserInfoMock {

    public static UserInfo createMock(int id, UserLogistics logistics) {
        UserInfo info = new UserInfo();
        info.setId(String.valueOf(id));
        info.setLogistics(logistics);
        info.setUserName("测试" + id);
        return info;
    }

    public static UserInfo createMock(int id, int type) {
        UserInfo info = new UserInfo();
        info.setId(String.valueOf(id));
        info.setLogistics(null);
        info.setType(type);
        info.setUserName("测试" + id);
        return info;
    }

    public static UserInfo createMock(int id, String groupName) {
        UserInfo info = new UserInfo();
        info.setId(String.valueOf(id));
        info.setLogistics(null);
        info.setGroupName(groupName);
        info.setType(0);
        info.setUserName("测试" + id);
        return info;
    }

}
