package dai.samples.mongodb.mock;


import dai.samples.mongodb.entity.UserLogistics;

public class UserLogisticsMock {

    public static UserLogistics createMock(int id, int type) {
        UserLogistics logistics = new UserLogistics();
        logistics.setId(String.valueOf(id));
        logistics.setType(type);
        logistics.setTel("测试Tel" + id);
        logistics.setAddress("测试Address" + id);
        return logistics;
    }
}
