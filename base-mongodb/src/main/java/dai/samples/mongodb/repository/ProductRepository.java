package dai.samples.mongodb.repository;

import dai.samples.mongodb.entity.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ProductRepository extends MongoRepository<Product,String> {

    void deleteAllByIdIn(List<String> idList);
}
