package dai.samples.mongodb.repository;

import dai.samples.mongodb.entity.UserLogistics;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserLogisticsRepository extends MongoRepository<UserLogistics,String> {

}
