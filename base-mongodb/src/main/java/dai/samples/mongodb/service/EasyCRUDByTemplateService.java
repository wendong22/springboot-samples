package dai.samples.mongodb.service;

import dai.samples.mongodb.entity.Product;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EasyCRUDByTemplateService {

    /**
     * 单一保存
     * @param product
     */
    void saveOneByTemplate(Product product);


    /**
     * 单一更新
     * @param product
     */
    void updateOneByTemplate(Product queryProduct, Product product);

    /**
     * 单一查询
     * @param id
     */
    Product queryByIdByTemplate(String id);

    /**
     * 根据实体查询
     * @param product
     */
    List<Product> queryByEntityByTemplate(Product product);

    /**
     * 根据实体分页查询
     * @param product
     */
    Page<Product> pageByEntityByTemplate(Product product, long startNum, int size);

    /**
     * 单一删除
     * @param id
     */
    void deleteOneByTemplate(String id);

    /**
     * 批量删除
     * @param idList
     */
    void batchDeleteByTemplate(List<String> idList);

}
