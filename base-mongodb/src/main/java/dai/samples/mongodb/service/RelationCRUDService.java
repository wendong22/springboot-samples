package dai.samples.mongodb.service;

import dai.samples.mongodb.entity.Order;
import dai.samples.mongodb.entity.UserInfo;

public interface RelationCRUDService {

    /**
     * 关联保存
     * @param userInfo
     */
    void saveUserInfo(UserInfo userInfo);

    /**
     * 关联保存
     * @param order
     */
    void saveOrder(Order order);

    /**
     * 关联查询
     * @param id
     */
    UserInfo queryUserInfo(String id);

    /**
     * 关联查询
     * @param id
     */
    Order queryOrder(String id);

    /**
     * 关联删除
     * @param id
     */
    void deleteUserInfo(String id);

    /**
     * 关联删除
     * @param id
     */
    void deleteOrder(String id);

}
