package dai.samples.mongodb.service.impl;

import dai.samples.mongodb.entity.Product;
import dai.samples.mongodb.service.EasyCRUDByTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 使用MongoTemplate的相关例子
 */
@Service
public class EasyCRUDByTemplateServiceImpl implements EasyCRUDByTemplateService {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void saveOneByTemplate(Product product) {
        mongoTemplate.save(product);
    }


    /**
     * 执行插入。如果没有找到匹配查询的文档，则创建并插入一个新文档
     * @param queryProduct
     * @param product
     */
    @Override
    public void updateOneByTemplate(Product queryProduct,Product product) {

        Query query = new Query();
        Criteria criteria = Criteria.byExample(Example.of(queryProduct));
        query.addCriteria(criteria);
        Update update = new Update();
        update.set("id",product.getId());
        update.set("productName",product.getProductName());
        update.set("productMoney",product.getProductMoney());
        mongoTemplate.updateMulti(query, update,Product.class);
    }


    @Override
    public Product queryByIdByTemplate(String id) {
        return mongoTemplate.findById(id,Product.class);
    }

    @Override
    public List<Product> queryByEntityByTemplate(Product product) {
        Query query = new Query();
        Criteria criteria = Criteria.byExample(Example.of(product));
        query.addCriteria(criteria);

        List<Product> products = mongoTemplate.find(query, Product.class);
        return products;
    }

    @Override
    public Page<Product> pageByEntityByTemplate(Product product, long startNum, int size) {
        Query query = new Query();
        Criteria criteria = Criteria.byExample(Example.of(product));
        query.addCriteria(criteria);
        long count = mongoTemplate.count(query, Product.class);
        query.skip(startNum).limit(size);
        List<Product> products = mongoTemplate.find(query, Product.class);
        Page page = new PageImpl(products, Pageable.unpaged(),count);
        return page;
    }

    @Override
    public void deleteOneByTemplate(String id) {
        Query query = new Query();
        Criteria criteria = Criteria.where("id").is(id);
        query.addCriteria(criteria);
        mongoTemplate.remove(query,Product.class);
    }

    @Override
    public void batchDeleteByTemplate(List<String> idList) {
        Query query = new Query();
        Criteria criteria = Criteria.where("id").in(idList);
        query.addCriteria(criteria);
        mongoTemplate.remove(query,Product.class);
    }
}
