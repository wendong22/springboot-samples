package dai.samples.mongodb.serivce.impl;

import dai.samples.MongodbApplication;
import dai.samples.mongodb.entity.Product;
import dai.samples.mongodb.mock.ProductMock;
import dai.samples.mongodb.repository.ProductRepository;
import dai.samples.mongodb.service.EasyCRUDByTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * 基础的增删查改使用MongoTemplate
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MongodbApplication.class)
@Slf4j
public class EasyCRUDByTemplateServiceImplTest {

    @Autowired
    private EasyCRUDByTemplateService templateService;

    @Autowired
    private ProductRepository productRepository;

    /**
     * 简单的保存
     */
    @Test
    public void saveOneByTemplate() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        Product mock = ProductMock.createMock(1);
        templateService.saveOneByTemplate(mock);
        Product product1 = productRepository.findById("1").get();
        Assert.assertNotNull(product1);
        Assert.assertTrue("测试1".equals(product1.getProductName()));
    }

    /**
     * 单一更新
     */
    @Test
    public void updateOneByTemplate() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        Product mock = ProductMock.createMock(1);
        templateService.saveOneByTemplate(mock);
        Product mock2 = ProductMock.createMock(1);
        mock2.setProductName("修改后内容");
        templateService.updateOneByTemplate(mock,mock2);
        Product product1 = productRepository.findById(mock.getId()).get();
        Assert.assertNotNull(product1);
        Assert.assertTrue("修改后内容".equals(product1.getProductName()));
    }

    /**
     * 单一查询
     */
    @Test
    public void queryByIdByTemplate() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        Product mock = ProductMock.createMock(1);
        templateService.saveOneByTemplate(mock);
        Product product = templateService.queryByIdByTemplate(mock.getId());
        Assert.assertNotNull(product);
        Assert.assertTrue(mock.equals(product));
    }

    /**
     * 根据对象条件查询
     */
    @Test
    public void queryByEntityByTemplate() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        Product mock = ProductMock.createMock(1);
        templateService.saveOneByTemplate(mock);
        List<Product> products = templateService.queryByEntityByTemplate(mock);
        Assert.assertNotNull(products);
        Assert.assertTrue(products.size() == 1);
        Assert.assertTrue(mock.equals(products.get(0)));
    }

    /**
     * 分页查询
     */
    @Test
    public void pageByEntityByTemplate() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        templateService.saveOneByTemplate(ProductMock.createMock(1));
        Product mock1 = ProductMock.createMock(1);
        mock1.setId("2");
        templateService.saveOneByTemplate(mock1);
        Product mock2 = ProductMock.createMock(1);
        mock2.setId("3");
        templateService.saveOneByTemplate(mock2);
        mock2.setId(null);
        Page<Product> products = templateService.pageByEntityByTemplate(mock2, 1, 2);
        Assert.assertNotNull(products);
        Assert.assertTrue(products.getTotalElements() == 3);
        Assert.assertTrue(products.getContent().size() == 2);
    }

    /**
     * 单一删除
     */
    @Test
    public void deleteOneByTemplate() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        Product mock = ProductMock.createMock(1);
        templateService.saveOneByTemplate(mock);
        templateService.deleteOneByTemplate(mock.getId());
        List<Product> all = productRepository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 0);
    }

    /**
     * 批量删除
     */
    @Test
    public void batchDeleteByTemplate() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        List<String> ids = new ArrayList<>();
        Product mock1 = ProductMock.createMock(1);
        ids.add(mock1.getId());
        templateService.saveOneByTemplate(mock1);
        Product mock2 = ProductMock.createMock(2);
        ids.add(mock2.getId());
        templateService.saveOneByTemplate(mock2);
        Product mock3 = ProductMock.createMock(3);
        ids.add(mock3.getId());
        templateService.saveOneByTemplate(mock3);
        templateService.batchDeleteByTemplate(ids);
        List<Product> all = productRepository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 0);
    }
}