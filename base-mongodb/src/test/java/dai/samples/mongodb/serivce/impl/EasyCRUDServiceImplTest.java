package dai.samples.mongodb.serivce.impl;

import dai.samples.MongodbApplication;
import dai.samples.mongodb.entity.Product;
import dai.samples.mongodb.mock.ProductMock;
import dai.samples.mongodb.repository.ProductRepository;
import dai.samples.mongodb.service.EasyCRUDService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 基础的增删查改使用MongoRepository
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MongodbApplication.class)
@Slf4j
public class EasyCRUDServiceImplTest {

    @Autowired
    private EasyCRUDService easyCRUDService;

    @Autowired
    private ProductRepository productRepository;

    /**
     * 简单的保存
     */
    @Test
    public void saveOne() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        Product mock = ProductMock.createMock(1);
        easyCRUDService.saveOne(mock);
        Product product1 = productRepository.findById("1").get();
        Assert.assertNotNull(product1);
        Assert.assertTrue("测试1".equals(product1.getProductName()));
    }

    /**
     * 批量保存
     */
    @Test
    public void batchSave() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        List<Product> mockList = ProductMock.createMockList(2);
        easyCRUDService.batchSave(mockList);
        List<Product> all = productRepository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 2);
    }

    /**
     * 单一更新
     */
    @Test
    public void updateOne() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        Product mock1 = ProductMock.createMock(1);
        easyCRUDService.saveOne(mock1);
        mock1.setProductName("修改后内容");
        easyCRUDService.updateOne(mock1);
        List<Product> all = productRepository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 1);
        Product product = all.get(0);
        Assert.assertTrue("修改后内容".equals(product.getProductName()));
    }

    /**
     * 批量更新
     */
    @Test
    public void batchUpdate() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        List<Product> mockList = ProductMock.createMockList(2);
        easyCRUDService.batchSave(mockList);
        mockList.forEach(item -> item.setProductName("修改后内容"));
        easyCRUDService.batchUpdate(mockList);
        List<Product> all = productRepository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 2);
        Product product = all.get(0);
        Assert.assertTrue("修改后内容".equals(product.getProductName()));

    }

    /**
     * 单一查询
     */
    @Test
    public void queryById() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        List<Product> mockList = ProductMock.createMockList(2);
        easyCRUDService.batchSave(mockList);
        Product product = mockList.get(0);
        Product product1 = easyCRUDService.queryById(product.getId());
        Assert.assertNotNull(product1);
        Assert.assertTrue(product1.equals(product));
    }

    /**
     * 根据对象条件查询
     */
    @Test
    public void queryByEntity() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        Product mock = ProductMock.createMock(1);
        easyCRUDService.saveOne(mock);
        List<Product> products = easyCRUDService.queryByEntity(mock);
        Assert.assertNotNull(products);
        Assert.assertTrue(products.size() == 1);
        Assert.assertTrue(mock.equals(products.get(0)));
    }

    /**
     * 分页查询
     */
    @Test
    public void pageByEntity() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        easyCRUDService.saveOne(ProductMock.createMock(1));
        Product mock1 = ProductMock.createMock(1);
        mock1.setId("2");
        easyCRUDService.saveOne(mock1);
        Product mock2 = ProductMock.createMock(1);
        mock2.setId("3");
        easyCRUDService.saveOne(mock2);
        mock2.setId(null);
        Page<Product> products = easyCRUDService.pageByEntity(mock2, 1, 2);
        Assert.assertNotNull(products);
        Assert.assertTrue(products.getTotalElements() == 3);
        Assert.assertTrue(products.getContent().size() == 2);
    }

    /**
     * 单一删除
     */
    @Test
    public void deleteOne() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        Product mock = ProductMock.createMock(1);
        easyCRUDService.saveOne(mock);
        easyCRUDService.deleteOne(mock.getId());
        List<Product> all = productRepository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 0);
    }

    /**
     * 批量删除
     */
    @Test
    public void batchDelete() {
        // 清除干扰数据
        productRepository.deleteAll();
        // 添加mock数据
        List<Product> mockList = ProductMock.createMockList(5);
        easyCRUDService.batchSave(mockList);
        List<String> ids = mockList.stream().map(Product::getId).collect(Collectors.toList());
        easyCRUDService.batchDelete(ids);
        List<Product> all = productRepository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 0);
    }
}