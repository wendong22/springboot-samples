package dai.samples.mongodb.serivce.impl;

import dai.samples.MongodbApplication;
import dai.samples.mongodb.entity.Order;
import dai.samples.mongodb.entity.OrderItem;
import dai.samples.mongodb.entity.UserInfo;
import dai.samples.mongodb.entity.UserLogistics;
import dai.samples.mongodb.mock.OrderItemMock;
import dai.samples.mongodb.mock.OrderMock;
import dai.samples.mongodb.mock.UserInfoMock;
import dai.samples.mongodb.mock.UserLogisticsMock;
import dai.samples.mongodb.repository.OrderRepository;
import dai.samples.mongodb.repository.UserInfoRepository;
import dai.samples.mongodb.repository.UserLogisticsRepository;
import dai.samples.mongodb.service.RelationCRUDService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


/**
 * 联表操作，其实是证明mongodb无法级联操作
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MongodbApplication.class)
@Slf4j
public class RelationCRUDServiceImplTest {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    UserInfoRepository userInfoRepository;

    @Autowired
    RelationCRUDService relationCRUDService;

    @Autowired
    UserLogisticsRepository logisticsRepository;

    /**
     * 保存用户信息，此时虽然logistics放入了用户信息中但是并未单独保存mongodb中
     * 所以最后查询的用户信息不包含logistics
     */
    @Test
    public void saveUserInfo() {
        // 清除干扰数据
        logisticsRepository.deleteAll();
        userInfoRepository.deleteAll();
        // 添加mock数据
        UserLogistics mock = UserLogisticsMock.createMock(1, 1);
        UserInfo userInfo = UserInfoMock.createMock(1, mock);
        relationCRUDService.saveUserInfo(userInfo);
        Optional<UserLogistics> byId = logisticsRepository.findById(mock.getId());
        try {
            UserLogistics logistics = byId.get();
        } catch (Exception e) {
            // 此时不包含logistics
            Assert.assertTrue(e instanceof NoSuchElementException);
        }
    }

    /**
     * 保存用户信息，此时logistics放入了用户信息前已经被保存在mongodb中
     * 所以最后查询的用户信息包含logistics
     */
    @Test
    public void saveUserInfo2() {
        // 清除干扰数据
        logisticsRepository.deleteAll();
        userInfoRepository.deleteAll();
        // 添加mock数据
        UserLogistics mock = UserLogisticsMock.createMock(1, 1);
        logisticsRepository.save(mock);
        UserInfo userInfo = UserInfoMock.createMock(1, mock);
        relationCRUDService.saveUserInfo(userInfo);
        UserLogistics logistics = logisticsRepository.findById(mock.getId()).get();
        // logistics 被单独保存此时存在logistics
        Assert.assertNotNull(logistics);
        Assert.assertTrue(mock.equals(logistics));
    }

    /**
     * 保存订单信息，此时OrderItem作为Order信息的一部分一起进行保存，而不是两个管理的集合
     */
    @Test
    public void saveOrder() {
        // 清除干扰数据
        orderRepository.deleteAll();
        // 添加mock数据
        OrderItem mock = OrderItemMock.createMock(1, "1", "1");
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(mock);
        Order mock1 = OrderMock.createMock(1, "1", 1, orderItems);
        relationCRUDService.saveOrder(mock1);
        Order order = orderRepository.findById(mock1.getId()).get();
        Assert.assertNotNull(order);
        List<OrderItem> orderItemList = order.getOrderItemList();
        Assert.assertNotNull(orderItemList);
        Assert.assertTrue(orderItemList.size() == 1);
        Assert.assertTrue(mock.equals(orderItemList.get(0)));
    }

    /**
     * 查询用户信息，此时mongodb中实际上用户保存着logistics的ID，但是查询的时候会查询关联的具体数据
     */
    @Test
    public void queryUserInfo() {
        // 清除干扰数据
        logisticsRepository.deleteAll();
        userInfoRepository.deleteAll();
        // 添加mock数据
        UserLogistics mock = UserLogisticsMock.createMock(1, 1);
        logisticsRepository.save(mock);
        UserInfo userInfo = UserInfoMock.createMock(1, mock);
        relationCRUDService.saveUserInfo(userInfo);
        UserInfo info = relationCRUDService.queryUserInfo(mock.getId());
        Assert.assertNotNull(info);
        UserLogistics logistics = info.getLogistics();
        Assert.assertNotNull(logistics);
        Assert.assertTrue(mock.equals(logistics));
    }

    /**
     * 查询订单，OrderItem会作为订单的内容一起输出
     */
    @Test
    public void queryOrder() {
        // 清除干扰数据
        orderRepository.deleteAll();
        // 添加mock数据
        OrderItem mock = OrderItemMock.createMock(1, "1", "1");
        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(mock);
        Order mock1 = OrderMock.createMock(1, "1", 1, orderItems);
        relationCRUDService.saveOrder(mock1);
        Order order = relationCRUDService.queryOrder(mock1.getId());
        Assert.assertNotNull(order);
        List<OrderItem> orderItemList = order.getOrderItemList();
        Assert.assertNotNull(orderItemList);
        Assert.assertTrue(orderItemList.size() == 1);
        Assert.assertTrue(mock.equals(orderItemList.get(0)));
    }

    /**
     * 删除用户信息的时候，并不会删除关联的logistics，需要单独删除
     */
    @Test
    public void deleteUserInfo() {
        // 清除干扰数据
        logisticsRepository.deleteAll();
        userInfoRepository.deleteAll();
        // 添加mock数据
        UserLogistics mock = UserLogisticsMock.createMock(1, 1);
        logisticsRepository.save(mock);
        UserInfo userInfo = UserInfoMock.createMock(1, mock);
        relationCRUDService.saveUserInfo(userInfo);
        // 此时删除用户信息
        relationCRUDService.deleteUserInfo(userInfo.getId());
        // 但是这时logistics依旧存在
        List<UserLogistics> all = logisticsRepository.findAll();
        Assert.assertNotNull(all);
        Assert.assertTrue(all.size() == 1);
        Assert.assertTrue(mock.equals(all.get(0)));

    }
}