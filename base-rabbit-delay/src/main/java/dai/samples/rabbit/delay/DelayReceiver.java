package dai.samples.rabbit.delay;

import dai.samples.rabbit.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * 消息监听
 * @author daify
 * @date 2019-07-18 17:52
 **/
@Component
@Slf4j
public class DelayReceiver {

    /**
     * queues是指要监听的队列的名字
     * @param user
     */
    @RabbitListener(queues = DelayConfig.PROCESS_QUEUE)
    @RabbitHandler
    public void receiveDirect1(User user) {
        log.info("消息已经接收，时间为：{}",new Timestamp(System.currentTimeMillis()));
        System.out.println("【converter-receiveDirect1监听到消息】" + user);
    }

}