package dai.samples.rabbit.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 *
 * @author daify
 * @date 2019-07-18 17:21
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private String id;
    
    private String name;
    
    private Integer age;

    /**
     * 重试次数
     */
    private Integer retry;
    
}
