package dai.samples.rabbit.retry;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.AMQP;
import dai.samples.rabbit.retry.config.RetryConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerErrorHandler;
import org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 重试的错误处理
 * @author daify
 * @date 2019-07-23 10:05
 **/
@Slf4j
@Component(value = "retryReceiverListenerErrorHandler")
public class RetryReceiverListenerErrorHandler implements RabbitListenerErrorHandler {
    
    private static ConcurrentSkipListMap<Object, AtomicInteger> map = new ConcurrentSkipListMap();

    @Autowired
    private RabbitTemplate rabbitTemplate;


    /**
     */
    @Override 
    public Object handleError(Message amqpMessage,
                                        org.springframework.messaging.Message <?> message,
                                        ListenerExecutionFailedException exception)
            throws Exception {
        log.error("消息接收发生了错误，消息内容:{},错误信息:{}",
                JSON.toJSONString(message.getPayload()), 
                JSON.toJSONString(exception.getStackTrace()));
        throw new AmqpRejectAndDontRequeueException("超出次数");
    }
}
