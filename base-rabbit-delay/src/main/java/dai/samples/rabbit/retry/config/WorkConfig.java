package dai.samples.rabbit.retry.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 用来处理业务
 * @author daify
 * @date 2019-08-14 15:48
 **/
@Configuration
public class WorkConfig {

    /**
     * 处理业务的队列
     */
    public final static String WORK_QUEUE = "retry.work.queue";

    /**
     * 处理业务的交换器
     */
    public final static String WORK_EXCHANGE = "retry.work.exchange";

    /**
     * 处理业务的路由key
     */
    public final static String WORK_KEY = "retry.work.key";


    /**
     * 处理业务的交换器
     * @return
     */
    @Bean 
    DirectExchange retryWorkExchange() {
        return new DirectExchange(WORK_EXCHANGE);
    }


    /**
     * 处理业务的队列
     * @return
     */
    @Bean
    public Queue retryWorkQueue() {
        return QueueBuilder
                .durable(WORK_QUEUE)
                .build();
    }



    /**
     * 绑定处理队列的数据监听工作
     * @return
     */
    @Bean
    public Binding workRetryBinding() {
        return BindingBuilder
                .bind(retryWorkQueue())
                .to(retryWorkExchange())
                .with(WORK_KEY);
    }

}
