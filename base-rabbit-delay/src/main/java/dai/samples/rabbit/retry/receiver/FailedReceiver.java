package dai.samples.rabbit.retry.receiver;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import dai.samples.rabbit.entity.User;
import dai.samples.rabbit.retry.config.FailedConfig;
import dai.samples.rabbit.retry.config.RetryConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消息监听
 * @author daify
 * @date 2019-07-18 17:52
 **/
@Component
@Slf4j
public class FailedReceiver {

    /**
     * queues是指要监听的队列的名字
     * @param user
     */
    @RabbitListener(queues = FailedConfig.FAILED_QUEUE,
            errorHandler = "retryReceiverListenerErrorHandler")
    public void receiveDirect(User user, Channel channel, Message message) throws Exception {
        try {
            log.info("【FailedReceiver监听到消息】" + JSON.toJSONString(user));
            log.info(" 人工处理");
        } catch (Exception e) {
            log.info("receiver error");
        }
    }
}