package dai.samples.rabbit.base.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.rabbit.base.entity.User;
import dai.samples.rabbit.base.manager.direct.DirectSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author daify
 * @date 2019-07-22 14:03
 **/
@RestController
@RequestMapping("direct")
public class DirectController {

    @Autowired
    private DirectSender sender;
    
    @RequestMapping(value = "send1",method = RequestMethod.GET)
    public String sendMessageV1() {
        User user = new User("1","Direct",100);
        sender.send1(user);
        return JSON.toJSONString(user);
    }

    @RequestMapping(value = "send2",method = RequestMethod.GET)
    public String sendMessageV2() {
        User user = new User("12","DirectV2",200);
        sender.send2(user);
        return JSON.toJSONString(user);
    }


    @RequestMapping(value = "send3",method = RequestMethod.GET)
    public String sendMessageV3() {
        User user = new User("12","DirectV3",200);
        sender.send2(user);
        return JSON.toJSONString(user);
    }
}
