package dai.samples.rabbit.base.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.rabbit.base.entity.User;
import dai.samples.rabbit.base.manager.fanout.FanoutSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author daify
 * @date 2019-07-22 14:33
 **/
@RestController
@RequestMapping("fanout")
public class FanoutController {

    @Autowired
    private FanoutSender sender;

    @RequestMapping(value = "send",method = RequestMethod.GET)
    public String sendMessageV1() {
        User user = new User("1","Fanout",100);
        sender.send(user);
        return JSON.toJSONString(user);
    }
}
