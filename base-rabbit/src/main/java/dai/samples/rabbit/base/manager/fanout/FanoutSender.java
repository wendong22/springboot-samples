package dai.samples.rabbit.base.manager.fanout;

import dai.samples.rabbit.base.entity.User;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息发送
 * @author daify
 * @date 2019-07-18 17:36
 **/
@Component
public class FanoutSender {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 消息发送到FanoutConfig.FANOUT_EXCHANGE交换机中
     * @param user
     */
    public void send(User user) {
        this.rabbitTemplate
                .convertAndSend(FanoutConfig.FANOUT_EXCHANGE, "", user);
    }
}
