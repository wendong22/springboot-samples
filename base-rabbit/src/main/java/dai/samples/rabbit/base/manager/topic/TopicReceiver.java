package dai.samples.rabbit.base.manager.topic;

import dai.samples.rabbit.base.entity.User;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消息监听
 * @author daify
 * @date 2019-07-18 17:53
 **/
@Component
public class TopicReceiver {
    /**
     * queues是指要监听的队列的名字
     * @param user
     */
    @RabbitListener(queues = TopicConfig.TOPIC_QUEUE1)
    public void receiveTopic1(User user) {
        System.out.println("【receiveTopic1监听到消息】" + user.toString());
    }
    @RabbitListener(queues = TopicConfig.TOPIC_QUEUE2)
    public void receiveTopic2(User user) {
        System.out.println("【receiveTopic2监听到消息】" + user.toString());
    }
}
