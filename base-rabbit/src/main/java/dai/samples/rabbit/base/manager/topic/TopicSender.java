package dai.samples.rabbit.base.manager.topic;

import dai.samples.rabbit.base.entity.User;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息发送
 * @author daify
 * @date 2019-07-18 17:52
 **/
@Component
public class TopicSender {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 第一个参数：TopicExchange名字
     * 第二个参数：Route-Key
     * 第三个参数：要发送的内容
     * @param user
     */
    public void send(User user) {
        this.rabbitTemplate.convertAndSend(
                TopicConfig.TOPIC_EXCHANGE,
                "dai.message", 
                user);
        this.rabbitTemplate.convertAndSend(
                TopicConfig.TOPIC_EXCHANGE, 
                "dai.dai", 
                user);
    }
}
