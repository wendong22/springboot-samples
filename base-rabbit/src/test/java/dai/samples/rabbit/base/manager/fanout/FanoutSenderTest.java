package dai.samples.rabbit.base.manager.fanout;

import dai.samples.rabbit.RabbitApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 *
 * @author daify
 * @date 2019-07-22 10:27
 **/
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RabbitApplication.class)
public class FanoutSenderTest {

    @Autowired
    private FanoutSender sender;

    @Test
    public void send() {
        /*User user = new User("2","Fanout",100);
        sender.send(user);*/
    }
}
