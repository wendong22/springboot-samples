package dai.samples.rabbit.callback.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.rabbit.callback.direct.BackSender;
import dai.samples.rabbit.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试消息回调
 * @author daify
 * @date 2019-07-22 14:03
 **/
@RestController
@RequestMapping("back")
public class BackController {

    @Autowired
    private BackSender sender;

    /**
     * 正常的请求测试
     * @return
     */
    @RequestMapping(value = "send1",method = RequestMethod.GET)
    public String sendMessageV1() {
        User user = new User("1","Direct",100);
        sender.send1(user);
        return JSON.toJSONString(user);
    }

    /**
     * 测试没有到达exchange
     * @return
     */
    @RequestMapping(value = "send2",method = RequestMethod.GET)
    public String sendMessageV2() {
        User user = new User("12","DirectV2",200);
        sender.send2(user);
        return JSON.toJSONString(user);
    }

    /**
     * 测试没有到达queue
     * @return
     */
    @RequestMapping(value = "send3",method = RequestMethod.GET)
    public String sendMessageV3() {
        User user = new User("12","DirectV3",200);
        sender.send3(user);
        return JSON.toJSONString(user);
    }
}
