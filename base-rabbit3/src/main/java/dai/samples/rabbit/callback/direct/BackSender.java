package dai.samples.rabbit.callback.direct;

import dai.samples.rabbit.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * 消息发送
 * @author daify
 * @date 2019-07-18 17:51
 **/
@Component
@Slf4j
public class BackSender {
    
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send1(User user) {
        // ID标识 这样在RabbitConfirmCallBack中【消息唯一标识】 就不为空了
        String uuid = UUID.randomUUID().toString();
        CorrelationData date = new CorrelationData(uuid);

        this.rabbitTemplate.convertAndSend(
                BackConfig.BACK_EXCHANGE,
                // routingKey
                "back.V1",
                user,
                date);
    }

    public void send2(User user) {
        this.rabbitTemplate.convertAndSend(
                BackConfig.BACK_EXCHANGE_NO,
                // routingKey
                "back.V2",
                user);
    }

    public void send3(User user) {
        this.rabbitTemplate.convertAndSend(
                BackConfig.BACK_EXCHANGE,
                // routingKey
                "back.V3",
                user);
    }
}
