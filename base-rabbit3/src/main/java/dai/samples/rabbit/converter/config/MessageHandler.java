package dai.samples.rabbit.converter.config;

import lombok.extern.slf4j.Slf4j;

/**
 * @author daify
 * @date 2019-07-22
 */
@Slf4j
public class MessageHandler {

    public void onMessage(String user){
        log.info("---------onMessage-------------");
        log.info(user);
    }

}
