package dai.samples.rabbit.converter.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.rabbit.converter.direct.ConverterSender;
import dai.samples.rabbit.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试消息转换器
 * @see dai.samples.rabbit.converter.config.ConverterListenerConfig  需要去掉注释开打注解
 * @author daify
 * @date 2019-07-22 14:03
 **/
@RestController
@RequestMapping("converter")
public class ConverterController {

    @Autowired
    private ConverterSender sender;
    
    @RequestMapping(value = "send1",method = RequestMethod.GET)
    public String sendMessageV1() {
        User user = new User("1","Converter",100);
        sender.send1(user);
        return JSON.toJSONString(user);
    }
}
