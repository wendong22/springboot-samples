package dai.samples.rabbit.converter.direct;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 它会把消息路由到那些binding key与routing key完全匹配的Queue中
 * 根据routing key routing到相应的queue，
 * \routing不到任何queue的消息扔掉；
 * 可以不同的key绑到同一个queue，也可以同一个key绑到不同的queue；
 * @author daify
 * @date 2019-07-22 9:33
 **/
@Configuration
public class ConverterConfig {
    
    /**
     * redirect模式
     */
    public static final String CONVERTER_QUEUE1 = "converter.queue1";
    public static final String CONVERTER_QUEUE2 = "converter.queue2";
    public static final String CONVERTER_EXCHANGE = "converter.exchange";
    
    @Bean
    public Queue getConverterQueue() {
        return new Queue(CONVERTER_QUEUE1);
    }

    @Bean
    public Queue getConverterQueue2() {
        return new Queue(CONVERTER_QUEUE2);
    }
    
    @Bean
    public DirectExchange getConverterExchange() {
        return new DirectExchange(CONVERTER_EXCHANGE);
    }

    /**
     * direct模式
     * 消息中的路由键（routing key）如果和 Binding 中的 binding key 一致， 
     * 交换器就将消息发到对应的队列中。路由键与队列名完全匹配
     * @return
     */
    @Bean
    public Binding converterBinding1() {
        return BindingBuilder
                // 设置queue
                .bind(getConverterQueue())
                // 绑定交换机
                .to(getConverterExchange())
                // 设置routingKey
                .with("converter.V1");
    }


    @Bean
    public Binding converterBinding2() {
        return BindingBuilder
                // 设置queue
                .bind(getConverterQueue2())
                // 绑定交换机
                .to(getConverterExchange())
                // 设置routingKey
                .with("converter.V2");
    }

}
