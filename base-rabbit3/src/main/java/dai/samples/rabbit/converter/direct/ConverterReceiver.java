package dai.samples.rabbit.converter.direct;

import dai.samples.rabbit.entity.User;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;


/**
 * 消息监听
 * @author daify
 * @date 2019-07-18 17:52
 **/
@Component
public class ConverterReceiver {

    /**
     * queues是指要监听的队列的名字
     * @param user
     */
    @RabbitListener(queues = ConverterConfig.CONVERTER_QUEUE1)
    @RabbitHandler
    public void receiveDirect1(User user) {

        System.out.println("【converter-receiveDirect1监听到消息】" + user);
    }

    /**
     * queues是指要监听的队列的名字
     * @param user
     */
    @RabbitListener(queues = ConverterConfig.CONVERTER_QUEUE2)
    @RabbitHandler
    public void receiveDirect2(User user) {

        System.out.println("【converter-receiveDirect2监听到消息】" + user);
    }
}