package dai.samples.rabbit.converter.direct;

import dai.samples.rabbit.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息发送
 * @author daify
 * @date 2019-07-18 17:51
 **/
@Component
@Slf4j
public class ConverterSender {
    
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send1(User user) {
        this.rabbitTemplate.convertAndSend(
                ConverterConfig.CONVERTER_EXCHANGE,
                // routingKey
                "converter.V1",
                user);
    }

}
