package dai.samples.rabbit.errhandler.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.rabbit.errhandler.direct.DirectSender;
import dai.samples.rabbit.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author daify
 * @date 2019-07-22
 **/
@RestController
@RequestMapping("err")
public class DirectController {

    @Autowired
    private DirectSender sender;

    /**
     * 发送一个最后出现错误的请求
     * @return
     */
    @RequestMapping(value = "send1",method = RequestMethod.GET)
    public String sendMessageV1() {
        User user = new User("1","Direct",100);
        sender.send1(user);
        return JSON.toJSONString(user);
    }

    /**
     * 发送一个正常的
     * @return
     */
    @RequestMapping(value = "send2",method = RequestMethod.GET)
    public String sendMessageV2() {
        User user = new User("12","DirectV2",200);
        sender.send2(user);
        return JSON.toJSONString(user);
    }

}
