package dai.samples.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author daify
 * @date 2019-07-29 9:21
 **/
@SpringBootApplication
public class DelayApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(DelayApplication.class);
        application.run(args);

    }
    
}
