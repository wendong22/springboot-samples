package dai.samples.redis.base.service;

import dai.samples.redis.base.service.impl.ListLocation;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.redis.connection.RedisGeoCommands;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author daify
 * @date 2019-07-16
 */
public interface RedisService {

    void remove(String key, long timeout, TimeUnit unit);

    /**
     * 设置参数
     * @param key
     * @param value
     */
    void setData(String key,String value);

    /**
     * 获取参数
     * @param key
     * @return
     */
    String getData(String key);

    void setExpire(String key, long timeout, TimeUnit unit);

    /**
     * 操作list的操作，类似Deque（双端队列）
     * @param key
     * @param value
     * @param location
     */
    void setListData(String key, 
                     String value,
                     ListLocation location);

    /**
     * 获得list的值，类似Deque（双端队列）
     * @param value
     * @param location
     * @return
     */
    Object getListData(String value, ListLocation location);

    void removeListData(String key);

    /**
     * 设置hash的值
     * @param key
     * @param hashKey
     * @param value
     */
    void setHashData(String key,String hashKey,String value);
    /**
     * 设置hash的值
     * @param key
     * @param map
     */
    void setHashData(String key, Map map);

    /**
     * 获得hash的值
     * @param key
     * @param hashKey
     * @return
     */
    Object getHashData(String key,String hashKey);

    /**
     * 设置ZSet的值
     * @param key
     * @param value
     * @param source
     */
    void setZsetData(String key,String value,Double source);

    /**
     * 获得ZSet中的值
     * @param key
     * @param source1
     * @param source2
     * @return
     */
    Set getZsetData(String key, Double source1, Double source2);

    /**
     * 设置set的值
     * @param key
     * @param value
     */
    void setSetData(String key,String value);

    /**
     * 获得set的值
     * @param key
     * @return
     */
    Object getSetData(String key);

    Object getSetDataCount(String key);

    void removeSetDataCount(String key);

    /**
     * 设置Geo的值
     * @param key
     * @param location
     */
    void setGeoData(String key, RedisGeoCommands.GeoLocation <Object> location);

    /**
     * 获得Geo的值
     * @param key
     * @param circle
     * @return
     */
    GeoResults getGeoData(String key, Circle circle);
    /**
     * 设置HyperLogLog的值
     * @param key
     * @param objs
     */
    void setHyperLogLogData(String key,Object... objs);

    /**
     * 获得HyperLogLog的值
     * @param key
     * @return
     */
    Long getHyperLogLogData(String key);

    /**
     * 带超时时间的参数设置
     * @param key
     * @param value
     * @param timeOut
     */
    void setDataTimeOut(String key, String value, Long timeOut);
}
