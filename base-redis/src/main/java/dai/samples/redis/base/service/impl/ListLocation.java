package dai.samples.redis.base.service.impl;

/**
 *
 * @author daify
 * @date 2019-08-06 17:50
 **/
public enum ListLocation {
    RIGHT,
    LEFT;
}