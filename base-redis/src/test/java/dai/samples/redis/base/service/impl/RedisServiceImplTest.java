package dai.samples.redis.base.service.impl;

import dai.samples.redis.RedisApplication;
import dai.samples.redis.base.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author daify
 * @date 2019-07-16
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RedisApplication.class)
@Slf4j
public class RedisServiceImplTest {

    @Autowired
    RedisService redisService;

    /**
     * 测试值的设置
     */
    @Test
    public void setData() {
        redisService.setData("learn","redis");
        String learn = redisService.getData("learn");
        Assert.assertEquals("redis",learn);
    }

    /**
     * 测试值的获取
     */
    @Test
    public void getData() {
        String learn = redisService.getData("learn");
        Assert.assertEquals("redis",learn);
    }

    @Test
    public void setListData() {
        String key = "a";
        redisService.removeListData(key);


        String value = "A";
        redisService.setListData(key,value,ListLocation.LEFT);
        String value1 = "B";
        redisService.setListData(key,value1,ListLocation.LEFT);

        Object listData = redisService.getListData(key, ListLocation.LEFT);
        Assert.assertEquals("B",listData);
        Object listData2 = redisService.getListData(key, ListLocation.LEFT);
        Assert.assertEquals("A",listData2);

    }

    @Test 
    public void setHashData() {
        HashMap<String,String> map  = new HashMap();
        map.put("a","A");
        map.put("b","B");
        map.put("c","C");
        map.put("d","D");
        redisService.setHashData("map",map);
        Object hashData = redisService.getHashData("map", "a");
        Assert.assertEquals("A",hashData);
        try {
            Thread.sleep(6000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Object hashData2 = redisService.getHashData("map", "a");
        log.info(String.valueOf(hashData2));
    }

    @Test 
    public void setHashData1() {
        redisService.setHashData("map","y","Y");
        Object hashData = redisService.getHashData("map", "y");
        Assert.assertEquals("Y",hashData);
    }

    @Test 
    public void setZsetData() {

        redisService.setZsetData("dai","B",2D);
        redisService.setZsetData("dai","A",1D);
        redisService.setZsetData("dai","D",4D);
        redisService.setZsetData("dai","C",3D);
        redisService.setZsetData("dai","F",6D);
        redisService.setZsetData("dai","E",5D);
        redisService.setZsetData("dai","G",7D);

        Set dai = redisService.getZsetData("dai", 3D, 6D);
        Assert.assertTrue(dai.size() == 4);
        Object[] objects = dai.toArray();
        Assert.assertTrue("C".equals(objects[0]));
        Assert.assertTrue("D".equals(objects[1]));
        Assert.assertTrue("E".equals(objects[2]));
        Assert.assertTrue("F".equals(objects[3]));
    }

    @Test
    public void setSetData() {
        redisService.setSetData("daiSet","B");
        redisService.setSetData("daiSet","A");
        redisService.setSetData("daiSet","D");
        redisService.setSetData("daiSet","C");
        redisService.setSetData("daiSet","F");
        redisService.setSetData("daiSet","E");
        redisService.setSetData("daiSet","G");

        Object dai = redisService.getSetData("daiSet");
        log.info(String.valueOf(dai));
        Object dai2 = redisService.getSetData("daiSet");
        log.info(String.valueOf(dai2));
        Object dai3 = redisService.getSetData("daiSet");
        log.info(String.valueOf(dai3));
        Object dai4 = redisService.getSetData("daiSet");
        log.info(String.valueOf(dai4));
        Object dai5 = redisService.getSetData("daiSet");
        log.info(String.valueOf(dai5));
    }


    @Test 
    public void setGeoData() {
        String key = "A";
        Point point = new Point(114.403629,30.475316);
        RedisGeoCommands.GeoLocation <Object> location =
                new RedisGeoCommands.GeoLocation <>("软件园",point);
        Point point2 = new Point(114.386119,30.473688);
        RedisGeoCommands.GeoLocation <Object> location2 =
                new RedisGeoCommands.GeoLocation <>("财经政法",point2);
        // 保存坐标
        redisService.setGeoData(key,location);
        redisService.setGeoData(key,location2);
        redisService.setExpire(key,5000L, TimeUnit.MILLISECONDS);
        // 查询指定坐标，距离500米,此时应该只有一个坐标
        Circle circle = new Circle(114.403629,30.475316,500);
        GeoResults geoData = redisService.getGeoData(key, circle);
        List content = geoData.getContent();
        Assert.assertTrue(content.size() == 1);
        
        // 查询指定坐标，距离5000米,此时应该只有两个坐标
        Circle circle2 = new Circle(114.403629,30.475316,5000);
        GeoResults geoData2 = redisService.getGeoData(key, circle2);
        List content2 = geoData2.getContent();
        Assert.assertTrue(content2.size() == 2);

        try {
            Thread.sleep(6000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        // 查询指定坐标，距离5000米,此时应该只有两个坐标
        Circle circle3 = new Circle(114.403629,30.475316,5000);
        GeoResults geoData3 = redisService.getGeoData(key, circle3);
        List content3 = geoData3.getContent();
        Assert.assertTrue(content3.size() == 0);
    }


    @Test 
    public void setHyperLogLogData() {
        String key = "loglog";
        // 在HyperLogLog中实际插入的内容为A-E
        String[] values = new String[] {"A","B","C","D","E","B","D"};
        redisService.setHyperLogLogData(key,values);
        redisService.setExpire(key,3000L, TimeUnit.MILLISECONDS);
        Long hyperLogLogData = redisService.getHyperLogLogData(key);
        Assert.assertTrue(hyperLogLogData == 5);
        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Long hyperLogLogData2 = redisService.getHyperLogLogData(key);
        Assert.assertTrue(hyperLogLogData2 == 0);
    }

    @Test
    public void setDataTimeOut() {
        redisService.setDataTimeOut("map","value",5000L);
        String map = redisService.getData("map");
        Assert.assertTrue("value".equals(map));

        try {
            Thread.sleep(6000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String map2 = redisService.getData("map");
        Assert.assertNull(map2);

    }
}
