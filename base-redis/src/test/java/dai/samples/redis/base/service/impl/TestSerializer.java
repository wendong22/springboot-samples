package dai.samples.redis.base.service.impl;

import dai.samples.core.entitiy.User;
import dai.samples.redis.RedisApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author daify
 * @date 2019-08-07
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RedisApplication.class)
@Slf4j
public class TestSerializer {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 启动RedisConfig配置的时候可以此测试可以通过，否则无法通过
     */
    @Test
    public void setUserByJackson2JsonRedisSerializer() {
        User user = new User();
        user.setAge(10);
        user.setName("user");
        user.setId(1L);
        redisTemplate.opsForValue().set("test",user);
        Object o = redisTemplate.opsForValue().get("test");
        Assert.assertTrue(o instanceof User);
        Assert.assertEquals("user",((User)o).getName());
    }

    /**
     * 启动RedisConfig配置的时候可以此测试可以无法通过，否则通过
     */
    @Test
    public void setIntKey() {
        redisTemplate.opsForValue().set(10L,"10");
        Object o = redisTemplate.opsForValue().get(10L);
        Assert.assertEquals("10",o);
    }

}
