package dai.samples.shiro.config;

/**
 * 一些公共常量
 * 踢出登录操作
 */
public class Constants {

    public static final String SESSION_FORCE_LOGOUT_KEY = "force_logout";

    public static final String USER_REALM = "user_realm";

    public static final String SESSION = "session";
}
