package dai.samples.shiro.config.cache;


import lombok.extern.log4j.Log4j2;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 一个简单的内存缓存
 * @author daify
 */
@Log4j2
public class MapCache implements Cache {


    private static Map<Object,Object> cache = new HashMap<>();

    @Override
    public Object get(Object o) throws CacheException {
        log.info("MapCache get:{}",o);
        return cache.get(o);
    }

    @Override
    public Object put(Object o, Object o2) throws CacheException {
        log.info("MapCache put:{},{}",o,o2);

        return cache.put(o,o2);
    }

    @Override
    public Object remove(Object o) throws CacheException {
        log.info("MapCache remove:{}",o);

        return cache.remove(o);
    }

    @Override
    public void clear() throws CacheException {
        log.info("MapCache clear");

        cache.clear();
    }

    @Override
    public int size() {
        log.info("MapCache size");

        return cache.keySet().size();
    }

    @Override
    public Set keys() {
        log.info("MapCache keys");

        return cache.keySet();
    }

    @Override
    public Collection values() {
        log.info("MapCache values");

        return cache.values();
    }
}
