package dai.samples.shiro.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 关于权限相关请求测试
 */
@RestController
public class AuthorizeController {

    /**
     * 注解需要指定角色限制
     * @return
     */
    @RequiresRoles("superadmin")
    @RequestMapping("/superadmin")
    public String superAdmin() {
        return "superadmin!";
    }


    /**
     * 注解需要指定权限限制
     * @return
     */
    @RequiresPermissions("resources1")
    @RequestMapping("/resources1")
    public String resources1() {
        return "resources1!";
    }

    /**
     * 只需要登录的
     * @return
     */
    @RequestMapping("/index")
    public String index() {
        return "index!";
    }

    /**
     * 只需要登录的
     * @return
     */
    @RequestMapping("/remember")
    public String remember() {
        return "remember!";
    }
}
