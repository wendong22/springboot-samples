package dai.samples.shiro.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 权限
 */
@Data
@TableName
public class Permissions implements Serializable {
    /**
     * 权限ID
     */
    @TableId
    private long permissionsId;
    /**
     * 权限名称
     */
    private String permissionsName;
}
