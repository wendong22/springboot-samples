package dai.samples.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import dai.samples.shiro.entity.Permissions;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface PermissionsMapper extends BaseMapper<Permissions> {

    List<Permissions> getPermissionsByRoleId(@Param("roleId") Long roleId);
}
