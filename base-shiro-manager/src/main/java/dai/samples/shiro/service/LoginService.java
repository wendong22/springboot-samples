package dai.samples.shiro.service;

import dai.samples.shiro.entity.User;

/**
 * 登录
 */
public interface LoginService {

    /**
     * 登录
     * @param user
     */
    void login(User user);

    /**
     * 登出
     * @param userId
     */
    void logout(long userId);
}
