package dai.samples.shiro.vo;

import dai.samples.shiro.entity.Permissions;
import dai.samples.shiro.entity.Role;
import lombok.Data;

import java.util.List;

/**
 * 角色数据视图
 */
@Data
public class RoleVo extends Role {

    /**
     *  角色权限
     */
    private List<Permissions> permissions;
}
