DROP TABLE IF EXISTS user;

CREATE TABLE user
(
	user_id BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
	login_name VARCHAR(30) NULL DEFAULT NULL COMMENT '登录名称',
	password INT(11) NULL DEFAULT NULL COMMENT '密码',
	PRIMARY KEY (user_id)
);

DROP TABLE IF EXISTS role;

CREATE TABLE role
(
	role_id BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
	role_name VARCHAR(30) NULL DEFAULT NULL COMMENT '角色名称',
	PRIMARY KEY (role_id)
);

DROP TABLE IF EXISTS permissions;

CREATE TABLE permissions
(
	permissions_id BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '权限ID',
	permissions_name VARCHAR(30) NULL DEFAULT NULL COMMENT '权限名称',
	PRIMARY KEY (permissions_id)
);

DROP TABLE IF EXISTS user_role;


CREATE TABLE user_role
(
    ur_id BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '记录ID',
	user_id BIGINT(20) NOT NULL  COMMENT '用户ID',
	role_id BIGINT(20) NOT NULL  COMMENT '角色ID',
	PRIMARY KEY (ur_id)
);

DROP TABLE IF EXISTS role_permissions;


CREATE TABLE role_permissions
(
    rp_id BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '记录ID',
    role_id BIGINT(20) NOT NULL  COMMENT '角色ID',
	permissions_id BIGINT(20) NOT NULL  COMMENT '权限ID',
	PRIMARY KEY (rp_id)
);

CREATE TABLE my_session
(
    id VARCHAR(100) NOT NULL COMMENT 'ID',
    start_timestamp timestamp NOT NULL  COMMENT '会话开始时间',
	last_access_time timestamp NOT NULL  COMMENT '最后访问时间',
	timeout BIGINT(20) NOT NULL  COMMENT '过期时间',
	host VARCHAR(100) NULL DEFAULT NULL   COMMENT '主机地址',
	attribute_json VARCHAR(1000) NULL DEFAULT NULL   COMMENT '属性JSON',

	PRIMARY KEY (id)
);
