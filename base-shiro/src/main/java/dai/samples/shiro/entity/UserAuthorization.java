package dai.samples.shiro.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 用户授权对象
 */
@Data
@TableName
public class UserAuthorization {

    /**
     * 记录ID
     */
    @TableId
    private Long urId;

    /**
     * 授权ID
     */
    private Long userIdSource;

    /**
     * 被授权ID
     */
    private Long userIdTarget;

}
