package dai.samples.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import dai.samples.shiro.entity.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper extends BaseMapper<Role> {

    List<Role> getRoleByUserId(@Param("userId") long userId);

}