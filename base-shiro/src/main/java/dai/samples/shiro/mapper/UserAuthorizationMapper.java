package dai.samples.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import dai.samples.shiro.entity.UserAuthorization;

public interface UserAuthorizationMapper extends BaseMapper<UserAuthorization> {
}
