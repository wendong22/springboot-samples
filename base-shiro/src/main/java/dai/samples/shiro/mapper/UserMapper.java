package dai.samples.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import dai.samples.shiro.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
