package dai.samples.shiro.service;

import dai.samples.shiro.entity.UserAuthorization;

import java.util.List;

public interface UserAuthorizationService {

    /**
     * 添加用户授权
     * @param userIdSource
     * @param userIdTarget
     */
    void addUserAuthorization(long userIdSource, long userIdTarget);

    /**
     * 删除用户授权
     * @param userIdSource
     * @param userIdTarget
     */
    void deleteUserAuthorization(long userIdSource, long userIdTarget);

    /**
     * 验证用户授权是否存在
     * @param userIdTarget
     * @param userIdTarget
     */
    boolean existsUserAuthorization(String loginName, long userIdTarget);

    /**
     * 通过source获取用户授权信息
     * @param loginName
     * @return
     */
    List<UserAuthorization> getUserAuthorizationBySource(String loginName);

    /**
     * 通过target获取被授权信息
     * @param loginName
     * @return
     */
    List<UserAuthorization> getUserAuthorizationByTarget(String loginName);


}
