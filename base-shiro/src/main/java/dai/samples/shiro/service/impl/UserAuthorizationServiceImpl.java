package dai.samples.shiro.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import dai.samples.shiro.entity.UserAuthorization;
import dai.samples.shiro.mapper.UserAuthorizationMapper;
import dai.samples.shiro.mapper.UserMapper;
import dai.samples.shiro.service.UserAuthorizationService;
import dai.samples.shiro.service.UserService;
import dai.samples.shiro.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserAuthorizationServiceImpl implements UserAuthorizationService {

    @Autowired
    private UserAuthorizationMapper userAuthorizationMapper;

    @Autowired
    private UserService userService;

    @Override
    public void addUserAuthorization(long userIdSource, long userIdTarget) {
        UserAuthorization authorization = new UserAuthorization();
        authorization.setUserIdSource(userIdSource);
        authorization.setUserIdTarget(userIdTarget);
        userAuthorizationMapper.insert(authorization);
    }

    @Override
    public void deleteUserAuthorization(long userIdSource, long userIdTarget) {
        UserAuthorization authorization = new UserAuthorization();
        authorization.setUserIdSource(userIdSource);
        authorization.setUserIdTarget(userIdTarget);

        QueryWrapper<UserAuthorization> queryWrapper = new QueryWrapper();
        queryWrapper.setEntity(authorization);

        userAuthorizationMapper.delete(queryWrapper);
    }

    @Override
    public boolean existsUserAuthorization(String loginName, long userIdTarget) {
        UserVo userByName = userService.getUserByName(loginName);
        if (userByName == null) {
            return false;
        }
        UserAuthorization authorization = new UserAuthorization();
        authorization.setUserIdSource(userIdTarget);
        authorization.setUserIdTarget(userByName.getUserId());

        QueryWrapper<UserAuthorization> queryWrapper = new QueryWrapper();
        queryWrapper.setEntity(authorization);
        return userAuthorizationMapper.selectCount(queryWrapper) > 0;
    }

    @Override
    public List<UserAuthorization> getUserAuthorizationBySource(String loginName) {
        UserVo userByName = userService.getUserByName(loginName);
        if (userByName == null) {
            return new ArrayList<>();
        }
        UserAuthorization authorization = new UserAuthorization();
        authorization.setUserIdSource(userByName.getUserId());

        QueryWrapper<UserAuthorization> queryWrapper = new QueryWrapper();
        queryWrapper.setEntity(authorization);

        return userAuthorizationMapper.selectList(queryWrapper);
    }

    @Override
    public List<UserAuthorization> getUserAuthorizationByTarget(String loginName) {
        UserVo userByName = userService.getUserByName(loginName);
        if (userByName == null) {
            return new ArrayList<>();
        }
        UserAuthorization authorization = new UserAuthorization();
        authorization.setUserIdTarget(userByName.getUserId());

        QueryWrapper<UserAuthorization> queryWrapper = new QueryWrapper();
        queryWrapper.setEntity(authorization);
        return userAuthorizationMapper.selectList(queryWrapper);
    }
}
