DELETE FROM user;

INSERT INTO user (user_id, login_name, password) VALUES
(1, 'zhangsan', '123456'),
(2, 'lisi', '123456'),
(3, 'wangwu', '123456'),
(4, 'zhaoliu', '123456');

DELETE FROM role;

INSERT INTO role (role_id, role_name) VALUES
(1, 'superadmin'),
(2, 'admin'),
(3, 'user');

DELETE FROM user_role;

INSERT INTO user_role (ur_id, user_id,role_id) VALUES
(1,1,1),
(2,2,2),
(3,3,2),
(4,4,3);

DELETE FROM permissions;

INSERT INTO permissions (permissions_id, permissions_name) VALUES
(1, 'resources1'),
(2, 'resources2'),
(3, 'resources3');

DELETE FROM role_permissions;

INSERT INTO role_permissions (rp_id, role_id,permissions_id) VALUES
(1, 1,1),
(2, 1,2),
(3, 1,3),
(4, 2,2),
(5, 2,3),
(6, 3,3);

DELETE FROM user_authorization;
INSERT INTO user_authorization (ur_id, user_id_source,user_id_target) VALUES
(1, 1,2),
(2, 1,3),
(3, 2,1),
(4, 3,1),
(5, 3,2),
(6, 3,4);


