package dai.samples.solr.bean;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.Dynamic;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.util.Map;

/**
 *
 * @author daify
 * @date 2019-07-18 17:59
 **/
@Data
@SolrDocument(collection = "demo")
public class User {

    @Field("id")
    private String userId;

    @Field("address")
    private String address;

    /**
     * 动态域
     * 最好将范型加上
     */
    @Dynamic 
    @Field("item_spec_*")
    private Map <String,String> specMap;
    
}
