package dai.samples.swagger.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 测试返回对象注解
 * @author daify
 * @date 2019-07-16
 **/
@Data
@ApiModel(value = "错误的返回值",description = "错误的返回值")
public class ErrorRest {
    /**
     * 返回code
     */
    @ApiModelProperty(name = "code",
            value = "错误的code",
            dataType = "java.lang.Integer",
            example = "500")
    private Integer code;

    /**
     * 返回错误信息
     */
    @ApiModelProperty(name = "name",
            value = "错误信息",
            dataType = "String",
            example = "错误数据")
    private String message;

    /**
     * 返回错误的结果集
     */
    @ApiModelProperty(name = "data",
            value = "错误数据",
            dataType = "Object",
            example = "{}")
    private String data;
}
