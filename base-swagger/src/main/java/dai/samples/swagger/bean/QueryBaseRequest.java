package dai.samples.swagger.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 请求对象注解
 * @author daify
 * @date 2019-07-15
 **/
@Data
@ApiModel(value = "基础的请求对象",description = "请求对象描述")
public class QueryBaseRequest {
    
    @ApiModelProperty(name = "id",
            value = "id的值",
            dataType = "java.lang.Long",
            example = "1")
    private Long id;
    
}
