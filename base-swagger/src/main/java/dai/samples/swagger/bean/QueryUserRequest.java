package dai.samples.swagger.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author daify
 * @date 2019-07-15 9:19
 **/
@Data
@ApiModel(value = "子类的请求对象",
        description = "请求对象描述",
        parent = QueryBaseRequest.class)
public class QueryUserRequest extends QueryBaseRequest {
    
    @ApiModelProperty(name = "name",
            value = "name值",
            dataType = "String",
            example = "默认显示的值",
            allowEmptyValue = true)
    private String name;

    @ApiModelProperty(name = "param1",
            value = "这个参数会被隐藏",
            dataType = "String",
            required = true,
            hidden = true)
    private String param1;


    @ApiModelProperty(name = "param2",
            value = "参数2，这个参数必填",
            dataType = "String",
            required = true,
            allowableValues = "值1,值2,值3")
    private String param2;
}
