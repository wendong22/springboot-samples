package dai.samples.swagger.bean;

import dai.samples.core.entitiy.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author daify
 * @date 2019-07-16 14:36
 **/
@Data
@ApiModel(value = "用户的包装类",description = "用户的包装类描述")
public class UserWrapper {

    @ApiModelProperty(name = "id",
            value = "id的值",
            dataType = "java.lang.Long")
    private Long id;

    @ApiModelProperty(name = "name",
            value = "name值",
            dataType = "String")
    private String name;
    @ApiModelProperty(name = "age",
            value = "age值",
            dataType = "java.lang.Integer")
    private Integer age;
    @ApiModelProperty(name = "money",
            value = "money值",
            dataType = "java.lang.Double")
    private Double money;

    public UserWrapper(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.age = user.getAge();
        this.money = user.getMoney();
    }
}
