package dai.samples.swagger.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试注解隐藏 hidden
 * @author daify
 * @date 2019-07-16 10:53
 **/
@Api(value = "控制器隐藏",
        tags = "控制器隐藏(此注解将被隐藏)",
        hidden = true)
@RestController
@RequestMapping("classHidden")
public class TestClassAnnotationsHiddenController {
    
}
