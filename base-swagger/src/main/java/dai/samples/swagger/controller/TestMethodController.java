package dai.samples.swagger.controller;

import com.alibaba.fastjson.JSON;
import dai.samples.core.entitiy.User;
import dai.samples.swagger.bean.ErrorRest;
import dai.samples.swagger.bean.UserWrapper;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

/**
 * 测试具体方法上的注解
 * @author daify
 * @date 2019-07-16 11:06
 **/
@Api(value = "测试请求方法注解",tags = "测试请求方法的注解内容")
@RestController
@RequestMapping("method")
@Slf4j
public class TestMethodController {

    /**
     * 测试请求参数添加描述
     * @param name
     * @param age
     * @return
     */
    @ApiOperation(value="参数请求(请求描述)", 
            notes="请求的注释(注释)",
            httpMethod = "POST",
            response = String.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name",value = "name的描述",defaultValue = "默认值",
                    dataTypeClass = String.class,paramType = "query",
                    example = "参数实例值"),
            @ApiImplicitParam(name = "age",value = "age的描述",allowableValues = "1,2,3",
                    dataTypeClass = Integer.class,paramType = "query")
    })
    @RequestMapping(value = "base",method = RequestMethod.POST)
    public String testBaseParams(String name,Integer age) {
        log.info("传入参数:name:{},age:{}",name,age);
        return "传入参数:name:"+name+",age:"+age;
    }

    /**
     * 测试对入参对象不添加注解
     * @param user
     * @return
     */
    @ApiOperation(value="参数为对象请求,此时不对参数加额外注释(请求描述)",
            notes="参数为对象请求的注释(注释)",
            httpMethod = "POST")
    @RequestMapping(value = "bean",method = RequestMethod.POST)
    public String testBeanParams(User user) {
        log.info("传入参数:",JSON.toJSONString(user));
        return "传入参数:"+JSON.toJSONString(user); 
    }

    /**
     * 测试对入参添加注解，返回对象不添加注解
     * @param request
     * @param name
     * @param age
     * @return
     */
    @ApiOperation(value="参数请求返回对象，此时对参数加额外注释(请求描述)",
            notes="参数请求返回对象的注释(注释)",
            httpMethod = "POST",
            responseHeaders = {
                @ResponseHeader(name="dai",description = "头信息的补充")
            })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name",value = "name的描述",defaultValue = "默认值",
                    dataTypeClass = String.class,paramType = "query",
                    example = "参数实例值"),
            @ApiImplicitParam(name = "age",value = "age的描述",allowableValues = "1,2,3",
                    dataTypeClass = Integer.class,paramType = "query")
    })
    @RequestMapping(value = "baseToBean",method = RequestMethod.POST)
    public User testBaseParamsReturnBean(HttpServletRequest request,
                                         String name, Integer age) {
        String learn = request.getHeader("dai");
        log.info("传入头信息:dai:{}",learn);
        log.info("传入参数:name:{},age:{}",name,age);
        User user = new User();
        user.setName(name);
        user.setAge(age);
        return user;
    }

    /**
     * 测试入参对象不添加注解，返回对象添加注解
     * @param user
     * @return
     */
    @ApiOperation(value="对象请求返回对象(请求描述)",
            notes="对象请求返回对象的注释(注释)",
            httpMethod = "POST",
            code = 201)
    @ApiResponses({
            @ApiResponse(code = 200,message = "成功",response = UserWrapper.class),
            @ApiResponse(code = 500,message = "服务器错误",response = ErrorRest.class)
    })
    @RequestMapping(value = "beanToBean",method = RequestMethod.POST)
    public UserWrapper testBeanParamsReturnBean(User user) {
        log.info("传入参数:",JSON.toJSONString(user));
        return new UserWrapper(user);
    }

    /**
     * 测试隐藏单个请求
     * @param user
     * @return
     */
    @ApiOperation(value="测试隐藏,此内容不会在文档中显示(请求描述)",
            notes="测试隐藏的注释(注释)",
            httpMethod = "POST",
            hidden = true)
    @RequestMapping(value = "testHidden",method = RequestMethod.POST)
    public User testHidden(User user) {
        log.info("传入参数:",JSON.toJSONString(user));
        return user;
    }

    /**
     * 测试单个请求放置到顶端
     * @param user
     * @return
     */
    @ApiOperation(value="这个接口会被放到顶层，并且存在返回内容描述(请求描述)",
            notes="测试隐藏的注释(注释)",
            httpMethod = "POST",
            tags = "这个接口会被放到顶层,当然对应控制器依然有")
    @RequestMapping(value = "testTags",method = RequestMethod.POST)
    public UserWrapper testTags(User user) {
        if (user.getId() == null) {
            throw new RuntimeException("error");
        }
        log.info("传入参数:",JSON.toJSONString(user));
        return new UserWrapper(user);
    }

    /**
     * 测试忽略的注释
     * @param user
     * @return
     */
    @ApiOperation(value="注解标注忽略(请求描述)",
            notes="注解标注忽略的注释(注释)",
            httpMethod = "POST")
    @ApiIgnore(value = "注解标注忽略")
    @RequestMapping(value = "testIgnore",method = RequestMethod.POST)
    public User testIgnore(User user) {
        log.info("传入参数:",JSON.toJSONString(user));
        return user;
    }

    @ApiOperation(value="GET请求注释(请求描述)",
            notes="GET请求注释(注释)",
            httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "id的描述",
                    defaultValue = "2333",
                    dataTypeClass = Integer.class,paramType = "path",
                    example = "2333")
    })
    @RequestMapping(value = "{id}",method = RequestMethod.GET)
    public String testPath(@PathVariable("id") Integer id) {
        log.info("传入参数:",JSON.toJSONString(id));
        return "传入参数:" + id;
    }
}
