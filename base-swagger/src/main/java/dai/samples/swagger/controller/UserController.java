package dai.samples.swagger.controller;

import dai.samples.core.entitiy.User;
import dai.samples.swagger.bean.QueryUserRequest;
import dai.samples.swagger.mock.UserSource;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 测试返回内容的注解
 * @author daify
 * @date 2019-07-15 9:35
 **/
@ApiIgnore(value = "注解标注忽略")
@Api("User相关请求控制器")
@RestController
@RequestMapping("user")
public class UserController {

    @ApiOperation(value="添加用户信息", notes="将用户信息保存起来")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "用户实体user", required = true, dataType = "User")
    })
    @RequestMapping(value = "add",method = RequestMethod.POST)
    public User addUser(User user){
        User put = UserSource.map.put(user.getId(), user);
        return put;
    }

    @RequestMapping(value = "update",method = RequestMethod.POST)
    public User updateUser(User user) {
        User put = UserSource.map.replace(user.getId(), user);
        return put;
    }

    @RequestMapping(value = "del",method = RequestMethod.DELETE)
    public User delUser(QueryUserRequest request) {
        User remove = UserSource.map.remove(request.getId());
        return remove;
    }

    @RequestMapping(value = "query",method = RequestMethod.GET)
    public User queryUser(QueryUserRequest request) {
        User user = UserSource.map.get(request.getId());
        return user;
    }

    @ApiOperation(value="获取用户信息", notes="根据id来获取用户详细信息")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "Integer", paramType = "path")
    @RequestMapping(value = "{id}",method = RequestMethod.GET)
    public User queryUserById(@PathVariable(value = "id") Integer id) {
        User user = UserSource.map.get(id);
        return user;
    }
    
}
