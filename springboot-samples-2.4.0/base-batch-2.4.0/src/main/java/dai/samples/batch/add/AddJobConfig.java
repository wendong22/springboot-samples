package dai.samples.batch.add;

import dai.samples.batch.entity.BatchEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.support.SimpleFlow;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JacksonJsonObjectReader;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

/**
 * 在job中设置多个流程
 * @author daify
 * @date 2020-11-15
 */
@Slf4j
@Configuration
public class AddJobConfig {

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;


    /**
     * 流程1
     * @return
     */
    @Bean("flow1")
    public Flow addFlow1() {
        return new FlowBuilder<SimpleFlow>("flow1")
                .start(addStep1())
                .build();
    }


    /**
     * 流程2
     * @return
     */
    @Bean("flow2")
    public Flow addFlow2() {
        return new FlowBuilder<SimpleFlow>("flow2")
                .start(addStep2())
                .build();
    }

    /**
     * 任务的流程控制
     * @param jobRepository
     * @return
     */
    @Bean("addJob")
    public Job addJob(JobRepository jobRepository) {
        // 在addFlow1 和 addFlow2中对数据进行处理并保存后，在success中进行再次处理
        return this.jobBuilderFactory.get("addJob")
                .repository(jobRepository)
                .start(addFlow1())
                .split(new SimpleAsyncTaskExecutor())
                .add(addFlow2())
                .next(success())
                .end()
                .build();
    }

    /**
     * 成功时执行的方法
     * @return
     */
    @Bean("addStepSuccess")
    public Step success() {
        return this.stepBuilderFactory.get("successStep")
                .<BatchEntity, BatchEntity>chunk(1)
                .<BatchEntity, BatchEntity>chunk(1)
                .reader(itemReader())
                .processor(getProcessor())
                .writer(itemWriter())
                .allowStartIfComplete(true)
                .build();
    }


    /**
     * 成功时执行的处理方法
     * @return
     */
    private ItemProcessor<BatchEntity, ? extends BatchEntity> addProcessor1() {
        return (ItemProcessor<BatchEntity, BatchEntity>) entity -> {
            System.out.println("+-------addProcessor1----------");
            String fullName = entity.getFirstName() + " add1 " +
                    entity.getLastName();
            entity.setFullName(fullName);
            System.out.println(fullName + ":" + entity.getAge());
            return entity;
        };
    }

    /**
     * 成功时执行的处理方法
     * @return
     */
    private ItemProcessor<BatchEntity, ? extends BatchEntity> addProcessor2() {
        return (ItemProcessor<BatchEntity, BatchEntity>) entity -> {
            System.out.println("+-------addProcessor2----------");
            String fullName = entity.getFirstName() + " add2 " +
                    entity.getLastName();
            entity.setFullName(fullName);
            System.out.println(fullName + ":" + entity.getAge());
            return entity;
        };
    }

    /**
     * 模拟一次数据处理过程
     * @return
     */
    @Bean("addStep1")
    public Step addStep1() {
        return this.stepBuilderFactory.get("addStep")
                .<BatchEntity, BatchEntity>chunk(1)
                .reader(itemReader())
                .processor(addProcessor1())
                .writer(itemWriter())
                .faultTolerant()
                .build();
    }

    /**
     * 模拟另外一次数据处理过程
     * @return
     */
    @Bean("addStep2")
    public Step addStep2() {
        return this.stepBuilderFactory.get("addStep")
                .<BatchEntity, BatchEntity>chunk(1)
                .reader(itemReader())
                .processor(addProcessor2())
                .writer(itemWriter())
                .faultTolerant()
                .build();
    }

    /**
     * JSON写数据
     * @return
     */
    public ItemWriter<BatchEntity> itemWriter() {
        String name = "processJob-" + System.currentTimeMillis();
        String patch = "target/test-outputs/" + name + ".json";
        return new JsonFileItemWriterBuilder<BatchEntity>()
                .jsonObjectMarshaller(new JacksonJsonObjectMarshaller<>())
                .resource(new FileSystemResource(patch))
                .name(name)
                .build();
    }

    /**
     * JSON读数据
     * @return
     */
    public ItemReader<BatchEntity> itemReader() {
        return new JsonItemReaderBuilder<BatchEntity>()
                .jsonObjectReader(new JacksonJsonObjectReader<>(BatchEntity.class))
                .resource(new ClassPathResource("data/batchJob.json"))
                .name("batchJobReader")
                .build();
    }

    public ItemProcessor<BatchEntity, ? extends BatchEntity> getProcessor() {
        return new ChangeNameProcessor();
    }


    /**
     * 处理过程
     */
    class ChangeNameProcessor implements ItemProcessor<BatchEntity, BatchEntity> {

        @Override
        public BatchEntity process(BatchEntity person) {
            System.out.println("+-------ChangeNameProcessor----------");
            String fullName =person.getFirstName() + " " +
                    person.getLastName();
            person.setFullName(fullName);
            System.out.println(fullName + ":" + person.getAge());
            return person;
        }
    }

}
