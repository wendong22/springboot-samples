package dai.samples.batch.allowstart;

import dai.samples.batch.entity.BatchEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JacksonJsonObjectReader;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;


/**
 * 测试任务可以重复执行
 * @author daify
 * @date 2020-11-03
 */
@Slf4j
@Component
public class AllowStartJobConfig {

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    @Autowired
    PlatformTransactionManager transactionManager;

    /**
     * 任务可以重复执行
     * @param jobRepository
     * @return
     */
    @Bean("allowStartJob")
    public Job allowStartJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("allowStartJob")
                .repository(jobRepository)
                .start(allowStartStep())
                .build();
    }

    /**
     * 设置任务可以重启
     * @param
     * @return
     */
    @Bean(value = "allowStartStep")
    public Step allowStartStep() {
        return this.stepBuilderFactory.get("allowStartStep")
                .transactionManager(transactionManager)
                .<BatchEntity, BatchEntity>chunk(20)
                .reader(itemReader())
                .processor(getProcessor())
                .writer(itemWriter())
                .allowStartIfComplete(true)
                .build();
    }

    /**
     * 任务不可以重复执行
     * @param jobRepository
     * @return
     */
    @Bean("allowFalseStartJob")
    public Job allowFalseStartJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("allowFalseStartJob")
                .repository(jobRepository)
                .start(allowFalseStartStep())
                .build();
    }

    /**
     * 设置任务不可重启
     * @return
     */
    @Bean(value = "allowFalseStartStep")
    public Step allowFalseStartStep() {
        return this.stepBuilderFactory.get("allowFalseStartStep")
                .transactionManager(transactionManager)
                .<BatchEntity, BatchEntity>chunk(20)
                .reader(itemReader())
                .processor(getProcessor())
                .writer(itemWriter())
                .allowStartIfComplete(false)
                .build();
    }


    @Bean("allowLimitStartJob")
    public Job allowLimitStartJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("allowLimitStartJob")
                .repository(jobRepository)
                .start(allowLimitStartStep())
                .build();
    }

    /**
     * 设置任务可以重启，但是只能重启2次
     * @return
     */
    @Bean(value = "allowLimitStartStep")
    public Step allowLimitStartStep() {
        return this.stepBuilderFactory.get("allowLimitStartStep")
                .transactionManager(transactionManager)
                .<BatchEntity, BatchEntity>chunk(20)
                .reader(itemReader())
                .processor(getProcessor())
                .writer(itemWriter())
                .startLimit(2)
                .allowStartIfComplete(true)
                .build();
    }



    /**
     * JSON写数据
     * @return
     */
    public ItemWriter<BatchEntity> itemWriter() {
        long time = System.currentTimeMillis();
        String name = "allow-" + time;
        String patch = "target/test-outputs/" + name + ".json";
        return new JsonFileItemWriterBuilder<BatchEntity>()
                .jsonObjectMarshaller(new JacksonJsonObjectMarshaller<>())
                .resource(new FileSystemResource(patch))
                .name(name)
                .build();
    }

    /**
     * JSON读数据
     * @return
     */
    public ItemReader<BatchEntity> itemReader() {
        return new JsonItemReaderBuilder<BatchEntity>()
                .jsonObjectReader(new JacksonJsonObjectReader<>(BatchEntity.class))
                .resource(new ClassPathResource("data/batchJob.json"))
                .name("batchJobReader")
                .build();
    }

    public ItemProcessor<? super BatchEntity, ? extends BatchEntity> getProcessor() {
        return new AllowStartJobConfig.ChangeNameProcessor();
    }


    /**
     * 处理过程
     */
    class ChangeNameProcessor implements ItemProcessor<BatchEntity, BatchEntity> {

        @Override
        public BatchEntity process(BatchEntity person) {
            String fullName = "allow-" + person.getFirstName() + " " +
                    person.getLastName();
            person.setFullName(fullName);
            log.info(fullName);
            return person;
        }
    }

}
