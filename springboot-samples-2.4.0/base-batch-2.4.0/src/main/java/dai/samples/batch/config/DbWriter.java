package dai.samples.batch.config;

import dai.samples.batch.entity.BatchEntity;
import dai.samples.batch.entity.BatchEntityRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author daify
 * @date 2020-12-25
 */
@Component
public class DbWriter implements ItemWriter<BatchEntity> {

    @Autowired
    BatchEntityRepository batchEntityRepository;

    @Override
    public void write(List<? extends BatchEntity> items) throws Exception {
        items.forEach(item -> {
            batchEntityRepository.save(item);
        });
    }
}
