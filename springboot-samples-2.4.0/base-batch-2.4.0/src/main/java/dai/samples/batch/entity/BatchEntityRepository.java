package dai.samples.batch.entity;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author daify
 * @date 2020-12-25
 */
public interface BatchEntityRepository extends JpaRepository<BatchEntity, String> {
}
