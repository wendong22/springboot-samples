package dai.samples.batch.listener;

import dai.samples.batch.entity.BatchEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JacksonJsonObjectReader;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * @author daify
 * @date 2020-11-15
 */
@Slf4j
@Configuration
public class ListenerJobConfig {


    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    @Autowired
    PlatformTransactionManager transactionManager;

    @Bean
    public MyJobExecutionListener myJobExecutionListener() {
        return new MyJobExecutionListener();
    }

    @Bean("listenerJob")
    public Job retrySkipJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("listenerJob")
                .repository(jobRepository)
                .start(listenerStep())
                .listener(myJobExecutionListener())
                .build();
    }

    @Bean
    public MyChunkListener myChunkListener() {
        return new MyChunkListener();
    }

    @Bean
    public MyItemReadListener myItemReadListener() {
        return new MyItemReadListener();
    }

    @Bean
    public MyStepExecutionListener myStepExecutionListener() {
        return new MyStepExecutionListener();
    }


    @Bean
    public MySkipListener mySkipListener() {
        return new MySkipListener();
    }

    @Bean
    public MyItemProcessListener myItemProcessListener() {
        return new MyItemProcessListener();
    }

    @Bean
    public MyItemWriterListener myItemWriterListener() {
        return new MyItemWriterListener();
    }

    /**
     * 遇见RuntimeException异常进行重试，重试3次
     * @return
     */
    @Bean("listenerStep")
    public Step listenerStep() {
        return this.stepBuilderFactory.get("listenerStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemReader())
                .processor(getProcessor())
                .writer(itemWriter())
                .listener(myChunkListener())// 块监听器
                .listener(myStepExecutionListener())// step监听器
                .listener(myItemReadListener())// 读取监听器
                .listener(myItemProcessListener())// 处理监听器
                .listener(myItemWriterListener())
                .faultTolerant()
                .skip(Exception.class) // 重试
                .skipLimit(100)
                .listener(mySkipListener())// 跳过监听器,注意此监听器需要在faultTolerant后面
                .build();
    }

    /**
     * JSON写数据
     * @return
     */
    public ItemWriter<BatchEntity> itemWriter() {
        String name = "listenerJob-" + System.currentTimeMillis();
        String patch = "target/test-outputs/" + name + ".json";
        return new JsonFileItemWriterBuilder<BatchEntity>()
                .jsonObjectMarshaller(new JacksonJsonObjectMarshaller<>())
                .resource(new FileSystemResource(patch))
                .name(name)
                .build();
    }

    /**
     * JSON读数据
     * @return
     */
    public ItemReader<BatchEntity> itemReader() {
        return new JsonItemReaderBuilder<BatchEntity>()
                .jsonObjectReader(new JacksonJsonObjectReader<>(BatchEntity.class))
                .resource(new ClassPathResource("data/batchJob.json"))
                .name("batchJobReader")
                .build();
    }

    public ItemProcessor<? super BatchEntity, ? extends BatchEntity> getProcessor() {
        return new ChangeNameProcessor();
    }


    /**
     * 处理过程
     */
    class ChangeNameProcessor implements ItemProcessor<BatchEntity, BatchEntity> {

        @Override
        public BatchEntity process(BatchEntity person) {
            String fullName =person.getFirstName() + " " +
                    person.getLastName();
            person.setFullName(fullName);
            System.out.println(fullName + ":" + person.getAge());
            if (person.getAge() > 10) {
                throw new RuntimeException();
            }
            return person;
        }
    }


}
