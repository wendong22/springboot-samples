package dai.samples.batch.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.scope.context.ChunkContext;


/**
 * 在事务范围内处理的项目。在每个提交间隔提交事务都会提交一个“块”。ChunkListener可以在块开始处理之前或块成功完成之后使用
 * @author daify
 * @date 2020-11-09
 */
@Slf4j
public class MyChunkListener implements ChunkListener {


    @Override
    public void beforeChunk(ChunkContext context) {
        log.info("-------------beforeChunk--------------");

    }

    @Override
    public void afterChunk(ChunkContext context) {
        log.info("-------------afterChunk--------------");

    }

    @Override
    public void afterChunkError(ChunkContext context) {
        log.info("-------------afterChunkError--------------");
    }
}
