package dai.samples.batch.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemReadListener;


/**
 * 读取监听器
 * @author daify
 * @date 2020-11-09
 */
@Slf4j
public class MyItemReadListener implements ItemReadListener {

    @Override
    public void beforeRead() {
        log.info("-------------beforeRead--------------");
    }

    @Override
    public void afterRead(Object o) {
        log.info("-------------afterRead--------------");
    }

    @Override
    public void onReadError(Exception e) {
        log.info("-------------onReadError--------------");

    }
}
