package dai.samples.batch.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemWriteListener;

import java.util.List;

/**
 * @author daify
 * @date 2020-11-15
 */
@Slf4j
public class MyItemWriterListener implements ItemWriteListener {

    @Override
    public void beforeWrite(List items) {
        log.info("-------------beforeWrite--------------");
    }

    @Override
    public void afterWrite(List items) {
        log.info("-------------afterWrite--------------");
    }

    @Override
    public void onWriteError(Exception exception, List items) {
        log.info("-------------onWriteError--------------");
    }
}
