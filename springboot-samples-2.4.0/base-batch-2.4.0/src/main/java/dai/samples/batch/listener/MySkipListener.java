package dai.samples.batch.listener;

import dai.samples.batch.entity.BatchEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.annotation.OnSkipInProcess;
import org.springframework.batch.core.annotation.OnSkipInRead;
import org.springframework.batch.core.annotation.OnSkipInWrite;

/**
 * 跳过监听器，每跳过一个项目就会调用一次
 * @author daify
 * @date 2020-11-09
 */
@Slf4j
public class MySkipListener {

    @OnSkipInRead
    public void onSkipInRead(Throwable throwable) {
        log.info("-------------onSkipInRead--------------");
    }

    @OnSkipInWrite
    public void onSkipInWrite(BatchEntity entity, Throwable throwable) {
        log.info("-------------onSkipInWrite--------------");
    }

    @OnSkipInProcess
    public void onSkipInProcess(BatchEntity entity, Throwable throwable) {
        log.info("-------------onSkipInProcess--------------");
    }
}
