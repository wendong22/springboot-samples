package dai.samples.batch.rw;

import com.alibaba.fastjson.JSON;
import dai.samples.batch.entity.BatchEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * 测试读取数据的JOB
 * @author daify
 * @date 2020-12-27
 */
@Slf4j
@Component
public class ReadJobConfig {


    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    /**
     * 单一文件行读取
     */
    @Autowired
    @Qualifier(value = "itemLineReader")
    FlatFileItemReader<BatchEntity> flatFileItemReader;

    /**
     * JSON读取
     */
    @Autowired
    @Qualifier(value = "itemJsonReader")
    ItemReader<BatchEntity> itemJsonReader;

    /**
     * XML读取
     */
    @Autowired
    @Qualifier(value = "itemXmlReader")
    StaxEventItemReader<BatchEntity> itemXmlReader;

    /**
     * 文件夹文件读取
     */
    @Autowired
    @Qualifier(value = "itemFileReader")
    MultiResourceItemReader<BatchEntity> itemFileReader;

    /**
     * JDBC读取
     */
    @Autowired
    @Qualifier(value = "itemJdbcReader")
    JdbcPagingItemReader<BatchEntity> itemJdbcReader;

    /**
     * JDBC读取
     */
    @Autowired
    @Qualifier(value = "itemJdbcCursorReader")
    JdbcCursorItemReader<BatchEntity> itemJdbcCursorReader;



    /**
     * 单一文件行读取
     * @param jobRepository
     * @return
     */
    @Bean("flatFileItemReaderJob")
    public Job flatFileItemReaderJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("flatFileItemReaderJob")
                .repository(jobRepository)
                .start(flatFileItemReaderStep())
                .build();
    }

    @Bean(value = "flatFileItemReaderStep")
    public Step flatFileItemReaderStep() {
        return this.stepBuilderFactory.get("flatFileItemReaderStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(flatFileItemReader)
                .processor(getProcessor())
                .writer(itemWriter())
                .allowStartIfComplete(true)
                .build();
    }


    /**
     * JSON读取
     * @param jobRepository
     * @return
     */
    @Bean("itemJsonReaderJob")
    public Job itemJsonReaderJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("itemJsonReaderJob")
                .repository(jobRepository)
                .start(itemJsonReaderStep())
                .build();
    }

    @Bean(value = "itemJsonReaderStep")
    public Step itemJsonReaderStep() {
        return this.stepBuilderFactory.get("itemJsonReaderStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemJsonReader)
                .processor(getProcessor())
                .writer(itemWriter())
                .allowStartIfComplete(true)
                .build();
    }

    /**
     * XML文件读取
     * @param jobRepository
     * @return
     */
    @Bean("itemXmlReaderJob")
    public Job itemXmlReaderJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("itemXmlReaderJob")
                .repository(jobRepository)
                .start(itemXmlReaderStep())
                .build();
    }


    @Bean(value = "itemXmlReaderStep")
    public Step itemXmlReaderStep() {
        return this.stepBuilderFactory.get("itemXmlReaderStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemXmlReader)
                .processor(getProcessor())
                .writer(itemWriter())
                .allowStartIfComplete(true)
                .build();
    }

    /**
     * 文件夹文件读取
     * @param jobRepository
     * @return
     */
    @Bean("itemFileReaderJob")
    public Job itemFileReaderJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("itemFileReaderJob")
                .repository(jobRepository)
                .start(itemFileReaderStep())
                .build();
    }


    @Bean(value = "itemFileReaderStep")
    public Step itemFileReaderStep() {
        return this.stepBuilderFactory.get("itemFileReaderStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemFileReader)
                .processor(getProcessor())
                .writer(itemWriter())
                .allowStartIfComplete(true)
                .build();
    }


    /**
     * Jdbc
     * @param jobRepository
     * @return
     */
    @Bean("itemJdbcReaderJob")
    public Job itemJdbcReaderJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("itemJdbcReaderJob")
                .repository(jobRepository)
                .start(itemJdbcReaderStep())
                .build();
    }


    @Bean(value = "itemJdbcReaderStep")
    public Step itemJdbcReaderStep() {
        return this.stepBuilderFactory.get("itemJdbcReaderStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemJdbcReader)
                .processor(getProcessor())
                .writer(itemWriter())
                .allowStartIfComplete(true)
                .build();
    }

    /**
     * Jdbc
     * @param jobRepository
     * @return
     */
    @Bean("itemJdbcCursorReaderJob")
    public Job itemJdbcCursorReaderJob(JobRepository jobRepository) {
        return this.jobBuilderFactory.get("itemJdbcCursorReaderJob")
                .repository(jobRepository)
                .start(itemJdbcCursorReaderStep())
                .build();
    }


    @Bean(value = "itemJdbcCursorReaderStep")
    public Step itemJdbcCursorReaderStep() {
        return this.stepBuilderFactory.get("itemJdbcCursorReaderStep")
                .<BatchEntity, BatchEntity>chunk(5)
                .reader(itemJdbcCursorReader)
                .processor(getProcessor())
                .writer(itemWriter())
                .allowStartIfComplete(true)
                .build();
    }

    /**
     * 处理
     * @return
     */
    private ItemProcessor<? super BatchEntity, ? extends BatchEntity> getProcessor() {
        return (ItemProcessor<BatchEntity, BatchEntity>) item -> {
            log.info(JSON.toJSONString(item));
            return item;
        };
    }

    /**
     * 写
     * @return
     */
    private ItemWriter<? super BatchEntity> itemWriter() {
        return (ItemWriter<BatchEntity>) item -> {};
    }


}
