package dai.samples.allow;

import dai.samples.batch.BatchApplication;
import dai.samples.batch.config.BatchConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Arrays;

/**
 * 测试任务可以重复执行
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BatchApplication.class})
@Slf4j
public class AllowJobTests {

    @Autowired
    @Qualifier(value = "allowStartJob")
    private Job allowStartJob;

    @Autowired
    @Qualifier(value = "allowFalseStartJob")
    private Job allowFalseStartJob;

    @Autowired
    @Qualifier(value = "allowLimitStartJob")
    private Job allowLimitStartJob;

    @Autowired
    private JobLauncher jobLauncher;

    /**
     * 任务可以重复执行，但是只能执行有限次数，超过次数限制则会测试失败
     * @throws Exception
     */
    @Test
    public void testAllowLimitStartJob() throws Exception {
        File file = new File(BatchConfig.PATH);
        Assert.isTrue(file.exists(),"地址错误");
        Assert.isTrue(file.isDirectory(),"目录错误");
        File[] files = file.listFiles();
        Arrays.stream(files).forEach(item -> item.delete());
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(allowLimitStartJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.FAILED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * 任务不可以重复执行，重复执行后并不会报错
     * @throws Exception
     */
    @Test
    public void testAllowFalseStartJob() throws Exception {

        File file = new File(BatchConfig.PATH);
        Assert.isTrue(file.exists(),"地址错误");
        Assert.isTrue(file.isDirectory(),"目录错误");
        File[] files = file.listFiles();
        Arrays.stream(files).forEach(item -> item.delete());
        JobExecution jobExecution = jobLauncher.run(allowFalseStartJob, new JobParameters());
        ExitStatus exitStatus = jobExecution.getExitStatus();
        String exitDescription = exitStatus.getExitDescription();
        log.info("exitDescription is {}",exitDescription);
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * 任务可以重复执行且没有限制
     * @throws Exception
     */
    @Test
    public void testAllowStartJob() throws Exception {

        File file = new File(BatchConfig.PATH);
        Assert.isTrue(file.exists(),"地址错误");
        Assert.isTrue(file.isDirectory(),"目录错误");
        File[] files = file.listFiles();
        Arrays.stream(files).forEach(item -> item.delete());
        JobExecution jobExecution = jobLauncher.run(allowStartJob, new JobParameters());
        ExitStatus exitStatus = jobExecution.getExitStatus();
        String exitDescription = exitStatus.getExitDescription();
        log.info("exitDescription is {}",exitDescription);
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

}
