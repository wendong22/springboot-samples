package dai.samples.retry;

import dai.samples.batch.BatchApplication;
import dai.samples.batch.config.BatchConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.io.File;
import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BatchApplication.class})
@Slf4j
public class RetryJobTests {

    @Autowired
    @Qualifier(value = "retryJob")
    private Job retryJob;

    @Autowired
    @Qualifier(value = "retrySkipJob")
    private Job retrySkipJob;

    @Autowired
    @Qualifier(value = "retryPolicyJob")
    private Job retryPolicy;

    @Autowired
    private JobLauncher jobLauncher;

    /**
     * 重试，会重试指定次数
     * @throws Exception
     */
    @Test
    public void testRetryJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(retryJob);

        File file = new File(BatchConfig.PATH);
        Assert.isTrue(file.exists(),"地址错误");
        Assert.isTrue(file.isDirectory(),"目录错误");
        File[] files = file.listFiles();
        Arrays.stream(files).forEach(item -> item.delete());
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
        File[] newFiles = file.listFiles();
        Arrays.stream(newFiles).forEach(item -> System.out.println(item.getName()));
    }


    /**
     * 重试，然后当重试次数超过限制值后，并不是中断任务，而是跳过。
     * 中间有个参数 chunk 可以尝试修改其数据，会发现他重试的不仅仅错误那一条数据，而是那一批
     * @throws Exception
     */
    @Test
    public void testRetrySkipJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(retrySkipJob);


        File file = new File(BatchConfig.PATH);
        Assert.isTrue(file.exists(),"地址错误");
        Assert.isTrue(file.isDirectory(),"目录错误");
        File[] files = file.listFiles();
        Arrays.stream(files).forEach(item -> item.delete());
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
        File[] newFiles = file.listFiles();
        Arrays.stream(newFiles).forEach(item -> System.out.println(item.getName()));
    }



    /**
     * 重试，然后当重试次数超过限制值后，并不是中断任务，而是跳过。
     * 中间有个参数 chunk 可以尝试修改其数据，会发现他重试的不仅仅错误那一条数据，而是那一批
     * @throws Exception
     */
    @Test
    public void testRetryPolicy() throws Exception {

        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(retryPolicy);


        File file = new File(BatchConfig.PATH);
        Assert.isTrue(file.exists(),"地址错误");
        Assert.isTrue(file.isDirectory(),"目录错误");
        File[] files = file.listFiles();
        Arrays.stream(files).forEach(item -> item.delete());
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
        File[] newFiles = file.listFiles();
        Arrays.stream(newFiles).forEach(item -> System.out.println(item.getName()));
    }
}
