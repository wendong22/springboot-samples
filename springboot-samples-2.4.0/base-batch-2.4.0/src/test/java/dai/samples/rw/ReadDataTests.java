package dai.samples.rw;

import dai.samples.batch.BatchApplication;
import dai.samples.batch.entity.BatchEntity;
import dai.samples.batch.entity.BatchEntityRepository;
import dai.samples.batch.mock.StartDataMock;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.List;

/**
 * 读取数据的测试
 * @author daify
 * @date 2020-12-27
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BatchApplication.class})
@Slf4j
public class ReadDataTests {

    @Autowired
    @Qualifier(value = "flatFileItemReaderJob")
    private Job flatFileItemReaderJob;

    @Autowired
    @Qualifier(value = "itemJsonReaderJob")
    private Job itemJsonReaderJob;

    @Autowired
    @Qualifier(value = "itemXmlReaderJob")
    private Job itemXmlReaderJob;

    @Autowired
    @Qualifier(value = "itemFileReaderJob")
    private Job itemFileReaderJob;

    @Autowired
    @Qualifier(value = "itemJdbcReaderJob")
    private Job itemJdbcReaderJob;

    @Autowired
    @Qualifier(value = "itemJdbcCursorReaderJob")
    private Job itemJdbcCursorReaderJob;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    BatchEntityRepository batchEntityRepository;


    /**
     * 单一文件行读取
     * @throws Exception
     */
    @Test
    public void testFlatFileItemReaderJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(flatFileItemReaderJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * JSON读取
     * @throws Exception
     */
    @Test
    public void testItemJsonReaderJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(itemJsonReaderJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * XML文件读取
     * @throws Exception
     */
    @Test
    public void testItemXmlReaderJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(itemXmlReaderJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * 文件夹文件读取
     * @throws Exception
     */
    @Test
    public void testItemFileReaderJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(itemFileReaderJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * Jdbc
     * @throws Exception
     */
    @Test
    public void testItemJdbcReaderJob() throws Exception {
        initData();
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(itemJdbcReaderJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * Jdbc
     * @throws Exception
     */
    @Test
    public void testItemJdbcCursorReaderJob() throws Exception {
        initData();
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(itemJdbcCursorReaderJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    private void initData() {
        batchEntityRepository.deleteAll();
        List<BatchEntity> entities = StartDataMock.list;
        batchEntityRepository.saveAll(entities);
    }
}
