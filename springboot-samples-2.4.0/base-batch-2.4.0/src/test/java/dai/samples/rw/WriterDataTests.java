package dai.samples.rw;

import com.alibaba.fastjson.JSON;
import dai.samples.batch.BatchApplication;
import dai.samples.batch.entity.BatchEntity;
import dai.samples.batch.entity.BatchEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.List;

/**
 * 读取数据的测试
 * @author daify
 * @date 2020-12-27
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BatchApplication.class})
@Slf4j
public class WriterDataTests {

    @Autowired
    @Qualifier(value = "itemJsonWriterJob")
    private Job itemJsonWriterJob;

    @Autowired
    @Qualifier(value = "itemLineWriterJob")
    private Job itemLineWriterJob;

    @Autowired
    @Qualifier(value = "itemXmlWriterJob")
    private Job itemXmlWriterJob;

    @Autowired
    @Qualifier(value = "itemJDBCWriterStep")
    private Job itemJDBCWriterStep;


    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    BatchEntityRepository batchEntityRepository;


    /**
     * JSON写数据
     * @throws Exception
     */
    @Test
    public void testItemJsonWriterJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(itemJsonWriterJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * 行格式写数据
     * @throws Exception
     */
    @Test
    public void testItemLineWriterJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(itemLineWriterJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * XML文件读取
     * @throws Exception
     */
    @Test
    public void testItemXmlWriterJob() throws Exception {
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(itemXmlWriterJob);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
    }

    /**
     * 文件夹文件读取
     * @throws Exception
     */
    @Test
    public void testItemJDBCWriterStep() throws Exception {
        batchEntityRepository.deleteAll();
        JobLauncherTestUtils jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJobLauncher(jobLauncher);
        jobLauncherTestUtils.setJob(itemJDBCWriterStep);
        JobExecution jobExecution = jobLauncherTestUtils.launchJob();
        Assert.isTrue(BatchStatus.COMPLETED.equals(jobExecution.getStatus()),"返回状态失败");
        List<BatchEntity> all = batchEntityRepository.findAll();
        all.forEach(item -> System.out.println(JSON.toJSONString(item)));
    }

}
