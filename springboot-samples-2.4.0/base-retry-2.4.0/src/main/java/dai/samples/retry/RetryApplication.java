package dai.samples.retry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

/**
 *
 * @author daify
 * @date 2021-01-04
 */
@SpringBootApplication
// 开启重试的支持，只是注解方式进行重试的时候需要
@EnableRetry
public class RetryApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetryApplication.class,args);
    }

}
