package dai.samples.retry.service;

import dai.samples.retry.RetryApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RetryApplication.class})
@Slf4j
public class RetryAnnotationServiceTest {

    @Autowired
    private RetryAnnotationService retryAnnotationService;

    @Test
    public void baseRetry() throws Exception {
        int store = retryAnnotationService.baseRetry(-1);
    }

    /**
     * 其配置的Recover方法和重试方法返回值不同，会报错
     * @throws Exception
     */
    @Test
    public void baseRetryError() throws Exception {
        int store = retryAnnotationService.baseRetryError(-1);
    }

}
